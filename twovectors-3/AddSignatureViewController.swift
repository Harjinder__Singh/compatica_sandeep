//
//  AddSignatureViewController.swift
//  twovectors-3
//
//  Created by Nick Bodnar on 11/3/15.
//  Copyright © 2015 Nick Bodnar. All rights reserved.
//

import Foundation
import UIKit

class AddSignatureViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    var delegate:AddSignatureViewControllerDelegate!
    
    @IBOutlet weak var label: UILabel!
    @IBOutlet weak var signature: YPDrawSignatureView!
    
    @IBAction func AddSignature(sender: UIButton, forEvent event: UIEvent) {
        delegate.done(self)
    }
    
    @IBAction func cancel(sender: UIButton, forEvent event: UIEvent) {
        delegate.cancel()
    }
    
    func getImage() -> UIImage? {
        return signature.getSignature()
    }
}