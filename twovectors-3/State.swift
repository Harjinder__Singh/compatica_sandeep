//
//  State.swift
//  twovectors-3
//
//  Created by Nick Bodnar on 9/25/15.
//  Copyright © 2015 Nick Bodnar. All rights reserved.
//

import Foundation
import CoreData
import UIKit

class State {
    static var debug = false
    static var clearData = false
    static var recorderVC:RecorderViewController!
    static var recorder:RecorderContainer!
    static var loginVC:LoginViewController!
    
    static var sessionUser:String = ""
    static var sessionCSRF:String = ""
    static var sessionId:String? {
        didSet {
            onSessionUpdated?()
        }
    }
    static var sessionTimeout:NSDate?
    
    static var onEmployeesUpdated:(() -> Void)?
    static var onLocationsUpdated:(() -> Void)?
    static var onZonesUpdated:(() -> Void)?
    static var onEquipmentsUpdated:(() -> Void)?
    static var onInjuriesUpdated:(() -> Void)?
    static var onIllnessesUpdated:(() -> Void)?
    static var onStatementsUpdated:(() -> Void)?
    static var onSessionUpdated:(() -> Void)?
    
    static var cacheLock = NSObject()
    static var cache = [String:[NSManagedObject]]()
    
    static func getObject(s:String) -> [NSManagedObject] {
        objc_sync_enter(State.cacheLock)
        defer{ objc_sync_exit(State.cacheLock) }

        if cache[s] == nil || cache[s]!.count == 0 {
            cache[s] = Storage.getEntities(s)
        }
        
        return cache[s]!
    }
    
    static func getEmployees() -> [Employee] {
        let s = "Employee"
        return getObject(s) as! [Employee]
    }
    
    static func getBodyParts() -> [BodyPart] {
        let s = "BodyPart"
        return getObject(s) as! [BodyPart]
    }
    
    static func getClinics() -> [Clinic] {
        let s = "Clinic"
        return getObject(s) as! [Clinic]
    }
    
    static func getClinic(id:Int64) -> Clinic? {
        for emp in getClinics() {
            if emp.id == id {
                return emp
            }
        }
        
        return nil
    }
    
    static func getER(id:Int64) -> ER? {
        for emp in getERs() {
            if emp.id == id {
                return emp
            }
        }
        
        return nil
    }
    
    static func getERs() -> [ER] {
        let s = "ER"
        return getObject(s) as! [ER]
    }
    
    static func getEmployee(id:Int64) -> Employee? {
        for emp in getEmployees() {
            if emp.id == id {
                return emp
            }
        }
        
        return nil
    }
    
    static func getEmployee(n:String) -> Employee? {
        for emp in getEmployees() {
            if emp.username == n {
                return emp
            }
        }
        
        return nil
    }
    
    static func getBodyPart(id:Int64) -> BodyPart? {
        for bp in getBodyParts() {
            if bp.id == id {
                return bp
            }
        }
        
        return nil
    }
    
    static func getInjuries() -> [Injury] {
        let s = "Injury"
        return getObject(s) as! [Injury]
    }
    
    static func getIllnesses() -> [Illness] {
        let s = "Illness"
        return getObject(s) as! [Illness]
    }
    
    static func getStatements() -> [Statement] {
        let s = "Statement"
        return getObject(s) as! [Statement]
    }
    
    static func getMedias() -> [Media] {
        let s = "Media"
        return getObject(s) as! [Media]
    }
    
    static func getMedia(id:Int64) -> Media? {
        let m = Storage.getEntities("Media", NSPredicate(format: "id == %ld", id)) as! [Media]
        if m.count >= 1 {
            return m[0]
        }
        
        return nil
    }
    
    static func getMedia(url:String) -> Media {
        objc_sync_enter(State.cacheLock)
        defer{ objc_sync_exit(State.cacheLock) }
        
        let m = Storage.getEntities("Media", NSPredicate(format: "url == %@", url)) as! [Media]
        if m.count >= 1 {
            return m[0]
        }

        let media = Storage.getBlankEntity("Media") as! Media
        
        media.id = Int64(arc4random_uniform(1000000000)) * -1
        media.type = StatementType.Camera.rawValue
        media.url = url
        Storage.save()
        
        return media
    }
    
    static func setOnEmployeesUpdated(updateFunction: () -> Void) {
        onEmployeesUpdated = updateFunction
    }
    
    static func setOnLocationsUpdated(updateFunction: () -> Void) {
        onLocationsUpdated = updateFunction
    }
    
    static func setOnZonesUpdated(updateFunction: () -> Void) {
        onZonesUpdated = updateFunction
    }
    
    static func setOnEquipmentsUpdated(updateFunction: () -> Void) {
        onEquipmentsUpdated = updateFunction
    }
    
    static func setOnInjuriesUpdated(updateFunction: () -> Void) {
        onInjuriesUpdated = updateFunction
    }
    
    static func setOnIllnessesUpdated(updateFunction: () -> Void) {
        onIllnessesUpdated = updateFunction
    }
    
    static func setOnStatementsUpdated(updateFunction: () -> Void) {
        onStatementsUpdated = updateFunction
    }
    
    static func setOnSessionUpdated(updateFunction: () -> Void) {
        onSessionUpdated = updateFunction
    }
    
    static func addEmployee(employee:Employee) {
        Storage.addEmployee(employee)
        onEmployeesUpdated?()
    }
    
    static func addLocation(location:Location) {
        Storage.addLocation(location)
        onLocationsUpdated?()
    }
    
    static func addZone(zone:Zone) {
        Storage.addZone(zone)
        onZonesUpdated?()
    }
    
    static func addEquipment(equipment:Equipment) {
        Storage.addEquipment(equipment)
        onEquipmentsUpdated?()
    }
    
    static func addClinic(clinic:Clinic) {
        Storage.addClinic(clinic)
    }
    
    static func addER(er:ER) {
        Storage.addER(er)
    }
    
    static func addInjury(i:Injury) {
        Storage.addInjury(i)
        onInjuriesUpdated?()
    }
    
    static func addIllness(i:Illness) {
        Storage.addIllness(i)
        onIllnessesUpdated?()
    }
    
    static func addBodyPart(major:String, _ minor:String) {
        Storage.addBodyPart(major, minor)
    }
    
    static func addStatement(incidentId:Int64, _ data:NSData, _ type:Int16, _ madeBy:Int64) {
        Storage.addStatement(incidentId, data, type, madeBy)
        onStatementsUpdated?()
    }
    
    static func getIncidents() -> [Incident] {
        let s = "Incident"
        return getObject(s) as! [Incident]
    }
    
    static func getLocations() -> [Location] {
        let s = "Location"
        return getObject(s) as! [Location]
    }
    
    static func getZones() -> [Zone] {
        let s = "Zone"
        return getObject(s) as! [Zone]
    }
    
    static func getZonesSorted() -> [Int64:[Zone]] {
        var result = [Int64:[Zone]]()
        let zones = getZones()
        
        for z in zones {
            result[z.location] = [Zone]()
        }
        
        for z in zones {
            result[z.location]!.append(z)
        }
        
        return result
    }
    
    static func getEquipments() -> [Equipment] {
        let s = "Equipment"
        return getObject(s) as! [Equipment]
    }
    
    static func getEquipmentsSorted() -> [Int64:[Equipment]] {
        var result = [Int64:[Equipment]]()
        let equipments = getEquipments()
        
        for e in equipments {
            result[e.zonex] = []
        }
        
        for e in equipments {
            result[e.zonex]!.append(e)
        }
        
        return result
    }
    
    static func getIncident(id:Int64) -> Incident? {
        if id == 0 {
            return nil
        }
        
        for inc in getIncidents() {
            if inc.id == id {
                return inc
            }
        }
        
        return nil
    }
    
    static func getLocation(id:Int64) -> Location? {
        for loc in getLocations() {
            if loc.id == id {
                return loc
            }
        }
        
        return nil
    }
    
    static func getZone(id:Int64) -> Zone? {
        for z in getZones() {
            if z.id == id {
                return z
            }
        }
        
        return nil
    }
    
    static func getEquipment(id:Int64) -> Equipment? {
        for e in getEquipments() {
            if e.id == id {
                return e
            }
        }
        
        return nil
    }
    
    static func newIncident(employeeId:Int64, _ time:NSTimeInterval) {
        setCurrentIncident(Storage.addIncident(employeeId, time).id)
    }
    
    static func submit() {
        currentIncident.submitted = true
        Storage.save()
    }
    
    static var currentIncident:Incident!

    static func setCurrentIncident(id:Int64) {
        currentIncident = getIncident(id)
    }
    
    static func initData() {
        if clearData {
            for e in State.getEmployees() {
                (UIApplication.sharedApplication().delegate as! AppDelegate).managedObjectContext?.deleteObject(e);
            }
            for e in State.getLocations() {
                (UIApplication.sharedApplication().delegate as! AppDelegate).managedObjectContext?.deleteObject(e);
            }
            for e in State.getZones() {
                (UIApplication.sharedApplication().delegate as! AppDelegate).managedObjectContext?.deleteObject(e);
            }
            for e in State.getEquipments() {
                (UIApplication.sharedApplication().delegate as! AppDelegate).managedObjectContext?.deleteObject(e);
            }
            for e in State.getClinics() {
                (UIApplication.sharedApplication().delegate as! AppDelegate).managedObjectContext?.deleteObject(e);
            }
            for e in State.getERs() {
                (UIApplication.sharedApplication().delegate as! AppDelegate).managedObjectContext?.deleteObject(e);
            }
            for e in State.getIncidents() {
                (UIApplication.sharedApplication().delegate as! AppDelegate).managedObjectContext?.deleteObject(e);
            }
            for e in State.getMedias() {
                (UIApplication.sharedApplication().delegate as! AppDelegate).managedObjectContext?.deleteObject(e);
            }
            
            Storage.save()
        }
        
        let bodyParts = State.getBodyParts()
        
        let toAdd = ["Head":["Back of Head", "Left Eye", "Right Eye", "Left Ear", "Right Ear", "Nose", "Mouth"], "Left Arm":["Hand", "Forearm", "Upper Arm"],"Right Arm":["Hand", "Forearm", "Upper Arm"], "Body":["Back", "Chest", "Abdomen"], "Left Leg":["Thigh", "Lower Leg", "Foot"], "Right Leg":["Thigh", "Lower Leg", "Foot"]]
        
        for (major, minors) in toAdd {
            for minor in minors {
                var found = false
                for bp in bodyParts {
                    if major == bp.major && minor == bp.minor {
                        found = true
                        break
                    }
                }
                
                if !found {
                    State.addBodyPart(major, minor)
                }
            }
        }
    }
}
