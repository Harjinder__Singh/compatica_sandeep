//
//  SyncIncidents.swift
//  compatica
//
//  Created by Nick Bodnar on 1/24/16.
//  Copyright © 2016 Nick Bodnar. All rights reserved.
//

import Foundation
import UIKit

class SyncIncidents: Sync<Incident> {
    override func getClientList() -> [Incident] {
        var result = [Incident]()
        for i in State.getIncidents() {
            if i.submitted && i.employee > 0 && i.location > 0 && i.zonex > 0 && i.equipment > 0 {
                result.append(i)
            }
        }
        
        return result
    }
    
    override func clientNew(obj: Incident) {
        webRequest("incident/create/?json", obj, "POST") {
            error, data in
            if error != nil {
                obj.submitted = false
                Storage.save()
                return
            }
            
            if error == nil && data?.length < 200 {
                (UIApplication.sharedApplication().delegate as! AppDelegate).managedObjectContext?.deleteObject(obj);
            }
        }
    }
}
