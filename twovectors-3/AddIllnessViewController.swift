//
//  AddIllnessViewController.swift
//  twovectors-3
//
//  Created by Nick Bodnar on 12/23/15.
//  Copyright © 2015 Nick Bodnar. All rights reserved.
//

import Foundation
import UIKit

class AddIllnessViewController: DetailViewController, IllnessTypeSelectorViewControllerDelegate {
    var delegate:AddIllnessViewControllerDelegate!
    var illness:Illness!
    
    var image = UIImageView(image: UIImage(named: "illness.png"))
    var illnessTypeButton = UIButton()
    var illnessDescription = UITextView()
    var sev0 = UIButton()
    var sev1 = UIButton()
    var sev2 = UIButton()
    
    var severity:Int16 = 0
    
    override func loadView() {
        super.loadView()
        
        illnessTypeButton.setTitle("Tap to Select", forState: .Normal)
        
        sev0.setTitle("Record Only", forState: .Normal)
        sev1.setTitle("First Aid", forState: .Normal)
        sev2.setTitle("Severe", forState: .Normal)
        
        addImageDetail(image, "Photo\n(Tap to Change)")
        addButtonDetail(illnessTypeButton, "Illness Type", "selectIllnessType:")
        addSpace()
        addMultiButtonDetail([sev0, sev1, sev2], ["sev0:", "sev1:", "sev2:"])
        addSpace()
        addTextViewDetail(illnessDescription, "Description", true, 150)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if type != .Create {
            illnessTypeButton.setTitle(illness.type, forState: .Normal)
            
            if illness.severity == 0 {
                sev0(NSObject())
            }
            if illness.severity == 1 {
                sev1(NSObject())
            }
            if illness.severity == 2 {
                sev2(NSObject())
            }
            
            illnessDescription.text = illness.notes
        } else {
            dispatch_async(dispatch_get_main_queue()) {
                self.selectIllnessTypeInternal()
            }
        }
        
        colorButtons()
    }
    
    func selectIllnessType(sender: AnyObject) {
        selectIllnessTypeInternal()
    }
    
    func sev0(sender: AnyObject) {
        severity = 0
        colorButtons()
    }
    
    func sev1(sender: AnyObject) {
        severity = 1
        colorButtons()
    }
    
    func sev2(sender: AnyObject) {
        severity = 2
        colorButtons()
    }
    
    func colorButtons() {
        sev0.backgroundColor = Util.multiButtonDefaultColor
        sev1.backgroundColor = Util.multiButtonDefaultColor
        sev2.backgroundColor = Util.multiButtonDefaultColor
        if severity == 0 {
            sev0.backgroundColor = Util.multiButtonSelectedColor
        }
        if severity == 1 {
            sev1.backgroundColor = Util.multiButtonSelectedColor
        }
        if severity == 2 {
            sev2.backgroundColor = Util.multiButtonSelectedColor
        }
    }
    
    func selectIllnessTypeInternal() {
        let vc = IllnessTypeSelectorViewController()
        vc.delegate = self
        
        Util.modal(vc, self)
    }
    
    @nonobjc
    func done(illnessType: String?) {
        if illnessType != nil {
            illnessTypeButton.setTitle(illnessType, forState: .Normal)
        }
        cancel()
    }
    
    func cancel() {
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    override func done(sender: UITapGestureRecognizer) {
        if type == ViewType.Create {
            illness = Illness.newObject()
        }
        
        illness.incident = State.currentIncident.id
        illness.severity = severity
        illness.type = illnessTypeButton.titleForState(.Normal)
        illness.notes = illnessDescription.text
        
        delegate.done(illness)
    }
    
    override func cancel(sender: UITapGestureRecognizer) {
        delegate.cancel()
    }
}
