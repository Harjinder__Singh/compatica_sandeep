//
//  InjuryTypeListViewController.swift
//  twovectors-3
//
//  Created by Nick Bodnar on 1/17/16.
//  Copyright © 2016 Nick Bodnar. All rights reserved.
//

import Foundation
import UIKit

class InjuryTypeListViewController: ObjectCollectionViewController<InjuryTypeView> {
    
    required override init() {
        super.init()
        headerText = "Select Injury Type"
        itemSize = ObjectViewSizes.smallSize
    }
    
    override func updateData() -> [objectType] {
        let types = ["Laceration", "Concussion", "Amputation", "Muscular Strain", "Burn", "Other"]
        
        var result = [InjuryTypeView]()
        for t in types {
            let i = InjuryTypeView(frame: CGRectMake(0, 0, 150, 75))
            i.injuryType = t
            result.append(i)
        }
        
        return result
    }
}
