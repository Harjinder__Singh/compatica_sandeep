//
//  Statement.swift
//  twovectors-3
//
//  Created by Nick Bodnar on 10/1/15.
//  Copyright © 2015 Nick Bodnar. All rights reserved.
//

import Foundation
import UIKit
import CoreData

class Statement: NSManagedObject {    
    func getMediaType() -> String {
        switch State.getMedia(media)!.type {
        case 4:
            return "Signature"
        case 3:
            return "Audio"
        case 2:
            return "Text"
        case 1:
            return "Image"
        default:
            return "Image"
        }
    }
    
    func getMadeByName() -> String {
        return State.getEmployee(madeBy)!.name!
    }
}
