//
//  SyncEquipments.swift
//  compatica
//
//  Created by Nick Bodnar on 1/24/16.
//  Copyright © 2016 Nick Bodnar. All rights reserved.
//

import Foundation

class SyncEquipments: Sync<Equipment> {    
    override func getServerList(callback: ((ErrorType?, [JSON]?) -> Void)) {
        webRequest("equipment/?json&include=address", nil, "GET") {
            (err, j) in
            if err != nil {
                callback(err, nil)
            } else {
                callback(err, JSON(data: j!).array)
            }
        }
    }
    
    override func getClientList() -> [Equipment] {
        return State.getEquipments()
    }
    
    override func serverNew(obj: JSON) {
        let o = Equipment.newObject()
        o.loadJSON(obj)
        
        updatePic(o)
        
        State.addEquipment(o)
    }
    
    override func clientNew(obj: Equipment) {
        if obj.zonex > 0 {
            webRequest("equipment/create/?json", obj, "POST") {
                error, data in
                
            }
        }
    }
    
    override func update(server: JSON, _ client: Equipment) {
        if client.modified < server["modified_unix"].doubleValue {
            let oldId = client.id
            client.loadJSON(server)
            
            updatePic(client)
            
            if server["id"].int64Value != oldId {
                for i in State.getIncidents() {
                    if i.equipment == oldId {
                        i.equipment = server["id"].int64Value
                    }
                }
            }
            
            Storage.save()
        } else {
            webRequest("equipment/" + String(client.id) + "/update/?json", client, "POST") {
                error, data in
                
            }
        }
    }
    
    func updatePic(o:Equipment) {
        if let m = State.getMedia(o.media) {
            if m.data != nil {
                return
            } else {
                webRequest("media/" + m.url!, nil, "GET") {
                    (err, data) in
                    m.data = data
                    Storage.save()
                }
            }
        }
    }
    
    override func done(success:Bool) {
        if success {
            SyncClinics().sync()
        }
    }
}
