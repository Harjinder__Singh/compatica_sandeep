//
//  RecorderViewController.swift
//  twovectors-3
//
//  Created by Nick Bodnar on 9/8/15.
//  Copyright (c) 2015 Nick Bodnar. All rights reserved.
//

import Foundation
import UIKit

let viewControllerNames: [String] = ["recorderEmployee", "recorderLocation", "recorderInjury", "recorderResponse", "recorderStatement", "recorderSummary"]

let viewControllerClasses = [EmployeeListViewController.self]

let viewControllerReadableNames: [String] = ["Employee", "Location", "Injury", "Response", "Statement", "Summary"]

class RecorderViewController: UIPageViewController, UIPageViewControllerDelegate, UIPageViewControllerDataSource {
    
    init() {
        super.init(transitionStyle: .Scroll, navigationOrientation: .Horizontal, options: nil)
    }

    required convenience init?(coder: NSCoder) {
        self.init()
    }
    
    lazy var namesToViewControllers: [String:UIViewController?] = {
        var results = [String:UIViewController?]()
        for vcName in viewControllerNames {
            results[vcName] = nil
        }
        return results
    }()
    
    override var transitionStyle:UIPageViewControllerTransitionStyle {
        get {
            return .Scroll
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        delegate = self
    }
    
    override func viewWillAppear(animated: Bool) {
        let i = figureOutIndexBasedOnData()
        if i == 0 || viewControllers!.count == 0 {
            goToPage(i)
        }
        
        super.viewWillAppear(animated)
    }
    
    func goToAppropriatePage() {
        goToPage(figureOutIndexBasedOnData())
    }
    
    func goToPage(page: Int) {
        if viewControllers!.count == 0 || (viewControllers![0] as! RecorderPage).leavingPage(page) {
            if State.currentIncident != nil {
                State.currentIncident.highestPage = max(State.currentIncident.highestPage, Int16(page))
            }
            
            setViewControllers([getVCForIndex(page)], direction: .Forward, animated: false, completion: nil)
        }
    }
    
    func nextPage() {
        let currentViewController = viewControllers![0]
        let nextViewController:UIViewController? = pageViewController(self, viewControllerAfterViewController: currentViewController)
        if let nvc = nextViewController {
            goToPage(getIndex(nvc))
        }
    }
    
    func prevPage() {
        let currentViewController = viewControllers![0]
        let prevViewController:UIViewController? = pageViewController(self, viewControllerBeforeViewController: currentViewController)
        if let pvc = prevViewController {
            goToPage(getIndex(pvc))
        }
    }
    
    override func setViewControllers(viewControllers: [UIViewController]?, direction: UIPageViewControllerNavigationDirection, animated: Bool, completion: ((Bool) -> Void)?) {
        viewControllers![0].view.frame = view.bounds
        viewControllers![0].view.autoresizingMask = [.FlexibleWidth, .FlexibleHeight]
        view.autoresizingMask = [.FlexibleWidth, .FlexibleHeight]
        
        let index = getIndex(viewControllers![0])
        
        State.recorder.previousButton.hidden = (index == 0)
        State.recorder.nextButton.hidden = (index == 5)
        
        super.setViewControllers(viewControllers, direction: direction, animated: animated, completion: completion)
        State.recorder.moveOverlay(index)
        State.recorder.setTopLabel(viewControllerReadableNames[index])
    }
    
    func pageViewController(pageViewController: UIPageViewController, viewControllerAfterViewController viewController: UIViewController) -> UIViewController? {
        let index = getIndex(viewController)
        if ( index == viewControllerNames.count - 1) {
            return nil
        }
        return getVCForIndex(index+1)
    }
    
    func pageViewController(pageViewController: UIPageViewController, viewControllerBeforeViewController viewController: UIViewController) -> UIViewController? {
        let index = getIndex(viewController)
        if ( index == 0) {
            return nil
        }
        
        return getVCForIndex(index-1)
    }
    
    func getIndex(vc: UIViewController) -> Int {
        return viewControllerNames.indexOf(vc.restorationIdentifier!)!
    }
    
    func figureOutIndexBasedOnData() -> Int {       
        if State.currentIncident != nil && State.currentIncident.employee != 0 {
            if State.currentIncident.equipment != 0 {
                return 2
            }
            return 1
        }
        
        return 0
    }
    
    func getVCForIndex(index: Int) -> UIViewController {
        if let vc = namesToViewControllers[viewControllerNames[index]] {
            return vc!
        }
        
        var vc:UIViewController
        
        // admitedly not elegant or scalable. Need to figure out swift arrays of generic types
        switch index {
        case 0:
            vc = RecorderEmployeeViewController()
        case 1:
            vc = RecorderLocationMetaViewController()
        case 2:
            vc = RecorderInjuryMetaViewController()
        case 3:
            vc = RecorderResponseMetaViewController()
        case 4:
            vc = RecorderStatementMetaViewController()
        case 5:
            vc = RecorderSummaryViewController()
        default:
            fatalError("No Page")
        }
        //let vc = viewControllerClasses[0].init()
        
        vc.restorationIdentifier = viewControllerNames[index]
        namesToViewControllers[viewControllerNames[index]] = vc
        return vc
    }
}
