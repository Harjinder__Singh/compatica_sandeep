//
//  AddZoneViewController.swift
//  twovectors-3
//
//  Created by Nick Bodnar on 12/23/15.
//  Copyright © 2015 Nick Bodnar. All rights reserved.
//

import Foundation
import UIKit

class AddZoneViewController: DetailViewController {
    var delegate:AddZoneViewControllerDelegate!
    @nonobjc
    var zone:Zone!
    var location:Location!
    
    var image = UIImageView(image: UIImage(named: "location2.png"))
    var name = UITextField()
    var notes = UITextField()
    var address = UITextField()
    var city = UITextField()
    var state = UITextField()
    var zip = UITextField()
    
    override func loadView() {
        super.loadView()
        
        if location.name != nil {
            addText("Zone for Location: " + location.name!)
        }
        addImageDetail(image, "Photo\n(Tap to Change)")
        addTextDetail(name, "Name")
        addTextDetail(notes, "Description")
        addTextDetail(address, "Address")
        addTextDetail(city, "City")
        addTextDetail(state, "State")
        addTextDetail(zip, "Zip Code")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if type != .Create {
            name.text = zone.name
            notes.text = zone.notes
            image.image = Util.getImage(zone.media)
            address.text = zone.address
            city.text = zone.city
            state.text = zone.state
            zip.text = zone.zip
        }
    }
    
    override func done(sender: UITapGestureRecognizer) {
        if type == ViewType.Create {
            zone = Zone.newObject()
        }
        
        zone.name = name.text
        zone.location = location.id
        zone.notes = notes.text
        zone.address = address.text
        zone.city = city.text
        zone.state = state.text
        zone.zip = zip.text
        
        delegate.done(zone)
    }
    
    override func cancel(sender: UITapGestureRecognizer) {
        delegate.cancel()
    }
}

