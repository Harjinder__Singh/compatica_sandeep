//
//  MajorBodyPartListViewController.swift
//  twovectors-3
//
//  Created by Nick Bodnar on 1/1/16.
//  Copyright © 2016 Nick Bodnar. All rights reserved.
//

import Foundation
import UIKit

class MajorBodyPartListViewController: ObjectCollectionViewController<MajorBodyPartView> {
    
    required override init() {
        super.init()
        headerText = "Select Major Body Part"
        itemSize = ObjectViewSizes.smallSize
    }
    
    override func updateData() -> [objectType] {
        let bodyParts = State.getBodyParts()
        
        var majors = [String:BodyPart]()
        
        for b in bodyParts {
            majors[b.major!] = b
        }
        
        var result = [MajorBodyPartView]()
        for (major, bp) in majors {
            let bpv = MajorBodyPartView(frame: CGRectMake(0, 0, 150, 75))
            bpv.bodyPart = bp
            if State.currentIncident.majorBodyPartIndicator == major {
                bpv.objectSelected = true
            }
            result.append(bpv)
        }
        
        return result
    }
}
