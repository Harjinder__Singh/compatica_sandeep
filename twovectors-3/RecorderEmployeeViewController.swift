//
//  RecorderEmployeeViewController.swift
//  twovectors-3
//
//  Created by Nick Bodnar on 1/1/16.
//  Copyright © 2016 Nick Bodnar. All rights reserved.
//

import Foundation
import UIKit

class RecorderEmployeeViewController: ObjectCollectionListViewController, RecorderPage {
    lazy var _vcs:[ObjectCollection] = {
        let oc:[ObjectCollection] = [EmployeeListViewController.init()]
        
        for o in oc {
            o.setParentList(self)
        }
        
        return oc
    }()
    
    override var vcs:[ObjectCollection]! {
        get {
            return _vcs
        }
    }
    
    var selectedEmployeeId:Int64?
    
    override func tapped(cell: ObjectView) {
        let e = cell as! EmployeeView
        selectedEmployeeId = e.objectSelected ? e.employee.id : nil
    }
    
    func pageStatus() -> RecorderPageStatus {
        if State.currentIncident == nil && selectedEmployeeId == nil {
            return .Unknown
        }
        
        return .Good
    }
    
    func leavingPage(toPage: Int) -> Bool {
        if let eid = selectedEmployeeId {
            State.newIncident(eid, NSDate.timeIntervalSinceReferenceDate())
            State.recorder.setTopRightLabel("Incident: " + State.getEmployee(eid)!.name!)
            
            selectedEmployeeId = nil
            return true
        }
        
        if State.currentIncident != nil {
            return true
        }
        
        return false
    }
}
