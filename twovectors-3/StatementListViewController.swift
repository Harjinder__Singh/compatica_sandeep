//
//  StatementListViewController.swift
//  twovectors-3
//
//  Created by Nick Bodnar on 12/20/15.
//  Copyright © 2015 Nick Bodnar. All rights reserved.
//

import Foundation
import UIKit

class StatementListViewController: ObjectCollectionViewController<StatementView>, AddStatementViewControllerDelegate {
    
    required override init() {
        super.init()
        detailBar = false
        addButtonHidden = false
        headerText = "Statement List"
    }
    
    override func setupDataCallbacks() {
        State.setOnStatementsUpdated(update)
    }
    
    override func updateData() -> [objectType] {
        let statements = State.currentIncident.getStatements()
        
        var result = [StatementView]()
        for s in statements {
            let sv = StatementView(frame: CGRectMake(0, 0, 150, 75))
            sv.statement = s
            result.append(sv)
        }
        
        return result
    }
    
    override func detail(cell: objectType) {
        let vc = AddStatementViewController()
        
        vc.viewType = .Edit
        vc.statement = cell.statement
        vc.delegate = self
        presentViewController(vc, animated: true, completion: nil)
    }
    
    override func add() {
        let vc = AddStatementViewController()
        vc.viewType = .Create
        vc.delegate = self
        presentViewController(vc, animated: true, completion: nil)
    }
    
    func done(vc:AddStatementViewController) {
        if let d = vc.data, mb = vc.madeBy, st = vc.statementType {
            if vc.viewType == ViewType.Create {
                State.addStatement(State.currentIncident.id, d, st.rawValue, mb.id)
                added()
            }
        }
        
        cancel()
    }
    
    func cancel() {
        dismissViewControllerAnimated(true, completion: nil)
    }
}
