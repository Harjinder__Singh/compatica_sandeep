//
//  ClinicSelectorViewController.swift
//  compatica
//
//  Created by Nick Bodnar on 2/4/16.
//  Copyright © 2016 Nick Bodnar. All rights reserved.
//

import Foundation
import UIKit

class ClinicSelectorViewController: ObjectCollectionListViewController {
    var delegate:ClinicOrERSelectorViewControllerDelegate!
    
    lazy var _vcs:[ObjectCollection] = {
        let oc:[ObjectCollection] = [ClinicListViewController.init(main: false)]
        
        for o in oc {
            o.setParentList(self)
        }
        
        return oc
    }()
    
    override var vcs:[ObjectCollection]! {
        get {
            return _vcs
        }
    }
    
    override func tapped(cell: ObjectView) {
        let e = cell as! ClinicView
        delegate.done(clinicOrER: e.clinic.id)
    }
}
