//
//  ClinicDetailViewController.swift
//  compatica
//
//  Created by Nick Bodnar on 2/15/16.
//  Copyright © 2016 Nick Bodnar. All rights reserved.
//

import Foundation
import UIKit

class ClinicDetailViewController: DetailViewController {
    var delegate:CancelDelegate!
    var x:Clinic!
    
    var name = UITextView()
    var doctor = UITextView()
    var telephone = UITextView()
    var notes = UITextView()
    var address = UITextView()
    var city = UITextView()
    var state = UITextView()
    var zip = UITextView()
    
    override func loadView() {
        super.loadView()
        
        addTextViewDetail(name, "Name")
        addMapView("Map", x.name ?? "", "Clinic", x.lat, x.lon, true)
        addTextViewDetail(doctor, "Doctor")
        addTextViewDetail(telephone, "Telephone")
        addTextViewDetail(notes, "Notes")
        addTextViewDetail(address, "Address")
        addTextViewDetail(city, "City")
        addTextViewDetail(state, "State")
        addTextViewDetail(zip, "Zip Code")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        name.text = x.name
        doctor.text = "Unknown"
        telephone.text = "(919)-555-2987"
        telephone.dataDetectorTypes = [.PhoneNumber]
        notes.text = x.notes
        address.text = x.address
        city.text = x.city
        state.text = x.state
        zip.text = x.zip
    }
    
    override func done(sender: UITapGestureRecognizer) {
        delegate.cancel()
    }
    
    override func cancel(sender: UITapGestureRecognizer) {
        delegate.cancel()
    }
}