//
//  Equipment.swift
//  twovectors-3
//
//  Created by Nick Bodnar on 10/1/15.
//  Copyright © 2015 Nick Bodnar. All rights reserved.
//

import Foundation
import CoreData

class Equipment: NSManagedObject, Syncable {
    
    func toJSON() -> JSON {
        var j = JSON([:])
        
        j["name"].string = name
        j["description"].string = notes
        j["zone"].int64 = zonex
        j["equipment_pic"].string = nil
        j["manager"].string = nil
        j["serial_number"].string = nil
        j["authorized_users"].arrayObject = [1]
        
        if let d = State.getMedia(media)?.data {
            j["base64_equipment_pic"].stringValue = d.base64EncodedStringWithOptions(NSDataBase64EncodingOptions(rawValue: 0))
        }

        return j
    }
    
    func compare(server: JSON) -> SyncCompare {
        let s = Storage.getBlankEntityInNoMOC("Equipment") as! Equipment
        s.loadJSON(server)
        
        let idMatch = id == s.id
        let nameMatch = name == s.name
        let zoneMatch = zonex == s.zonex
        if idMatch || (nameMatch && zoneMatch) {
            if idMatch && nameMatch && zoneMatch && notes == s.notes && media == s.media && modified == s.modified {
                return .Identical
            }
            
            return .SameObject
        }
        
        return .Different
    }
    
    func loadJSON(j: JSON) {
        modified = j["modified_unix"].doubleValue
        
        id = j["id"].int64Value
        name = j["name"].stringValue
        notes = j["description"].string
        zonex = j["zone"]["id"].int64Value
        media = State.getMedia(j["equipment_pic"].stringValue).id
    }
    
    static func newObject() -> Equipment {
        return Storage.getBlankEntity("Equipment") as! Equipment
    }
}
