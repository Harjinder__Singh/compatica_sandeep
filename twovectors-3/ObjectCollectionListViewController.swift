//
//  ObjectCollectionListViewController.swift
//  twovectors-3
//
//  Created by Nick Bodnar on 12/31/15.
//  Copyright © 2015 Nick Bodnar. All rights reserved.
//

import Foundation
import UIKit

class ObjectCollectionListViewController: UIViewController, ObjectCollectionList {
    var vcs:[ObjectCollection]! {
        get {
            return []
        }
    }
    
    var sv: UIScrollView = UIScrollView()
    
    override func loadView() {
        super.loadView()
        view.backgroundColor = UIColor.whiteColor()
        
        sv.frame = view.bounds
        sv.autoresizingMask = [.FlexibleWidth, .FlexibleHeight]
        
        for v in vcs {
            addChildViewController((v as! UIViewController))
            (v as! UIViewController).didMoveToParentViewController(self)
            sv.addSubview((v as! UIViewController).view)
        }

        view.addSubview(sv)
        
        for v in vcs {
            v.layout()
        }

        dispatch_async(dispatch_get_main_queue()) {
            self.resize()
        }
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        resize()
    }
    
    func resize() {
        var totalHeight = 0
        
        view.layoutIfNeeded()
        
        for v in vcs {
            let h = v.getHeight()
            v.setFrame(CGRectMake(0, CGFloat(totalHeight), view.bounds.width, h + 10))
            
            totalHeight = totalHeight + Int(h) + 10
        }

        sv.frame = view.bounds

        view.layoutIfNeeded()
        
        dispatch_async(dispatch_get_main_queue()) {
            Util.updateScrollViewSize(self.sv)
            
            dispatch_async(dispatch_get_main_queue()) {
                self.view.layoutIfNeeded()
            }
        }
    }
    
    func tapped(cell: ObjectView) {
        fatalError("I want to force overriding of this method right now. Reconsider this later.")
    }

    func updateCollections() {
        for v in vcs {
            v.update()
        }
    }
}