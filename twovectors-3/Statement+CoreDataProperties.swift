//
//  Statement+CoreDataProperties.swift
//  compatica
//
//  Created by Nick Bodnar on 3/14/16.
//  Copyright © 2016 Nick Bodnar. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension Statement {

    @NSManaged var id: Int64
    @NSManaged var incident: Int64
    @NSManaged var madeBy: Int64
    @NSManaged var madeByDesciptor: String?
    @NSManaged var media: Int64

}
