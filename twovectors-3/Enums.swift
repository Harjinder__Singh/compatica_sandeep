//
//  Enums.swift
//  twovectors-3
//
//  Created by Nick Bodnar on 12/5/15.
//  Copyright © 2015 Nick Bodnar. All rights reserved.
//

import Foundation

enum SyncCompare {
    case Different
    case SameObject
    case Identical
}

enum StatementType: Int16 {
    case Camera = 1
    case Text = 2
    case Audio = 3
    case Signature = 4
}

enum Severity: Int16 {
    case RecordOnly = 0
    case FirstAid = 1
    case Severe = 2
}

enum Answer: String {
    case Yes = "Yes"
    case No = "No"
    case Refused = "Employee Refuses Medical Attention"
}

enum Destination: String {
    case Work = "Work"
    case Home = "Home"
    case Clinic = "Clinic"
    case ER = "Hospital"
}

enum SignatureType {
    case MedicalRefusal
    case MedicalInfoRelease
}

enum RecorderPageStatus {
    case None
    case Good
    case Bad
    case Unknown
}

enum ViewType {
    case View
    case Create
    case Edit
}
