//
//  RecorderEquipmentViewController.swift
//  twovectors-3
//
//  Created by Nick Bodnar on 12/18/15.
//  Copyright © 2015 Nick Bodnar. All rights reserved.
//

import Foundation

import UIKit

class EquipmentListViewController: ObjectCollectionViewController<EquipmentView>, AddEquipmentViewControllerDelegate {
    
    required override init() {
        super.init()
        searchBarHidden = false
        addButtonHidden = false
        headerText = "Select Equipment"
    }
    
    override func setupDataCallbacks() {
        State.setOnEquipmentsUpdated(update)
    }
    
    override func updateData() -> [objectType] {
        var result = [EquipmentView]()
        headerText = ""
        let location = State.currentIncident.location
        let zone = State.currentIncident.zonex
        
        var noneFound = false
        
        if zone != 0 && location != 0 {
            headerText = "Select Related Equipment"
            let equipments = State.getEquipments()
            for e in equipments {
                if e.zonex == zone {
                    if e.name!.lowercaseString == "no equipment" {
                        noneFound = true
                    }
    
                    if filter == "" || e.name!.lowercaseString.containsString(filter.lowercaseString) {
                        let ev = EquipmentView(frame: CGRectMake(0, 0, 150, 75))
                        ev.equipment = e
                        if State.currentIncident.equipment == e.id {
                            ev.objectSelected = true
                            result = [EquipmentView]()
                            result.append(ev)
                            return result
                        }
                        result.append(ev)
                    }
                }
            }
        }
        
        if !noneFound && zone != 0 && location != 0 {
            let e = Equipment.newObject()
            e.name = "No Equipment"
            e.zonex = zone
            State.addEquipment(e)
            return updateData()
        }
        
        result.sortInPlace() {
            (a, b) in
            
            if a.equipment.name!.lowercaseString == "no equipment" {
                return true
            }
            
            return a.equipment.name?.lowercaseString < b.equipment.name?.lowercaseString
        }
        
        return result
    }
    
    override func detail(cell: objectType) {
        let add = AddEquipmentViewController()
        add.type = .Edit
        add.equipment = cell.equipment
        add.zone = State.getZone(cell.equipment.zonex)
        add.delegate = self
        presentViewController(add, animated: true, completion: nil)
    }
    
    override func add() {
        let add = AddEquipmentViewController()
        add.type = .Create
        add.zone = State.getZone(State.currentIncident.zonex)
        add.delegate = self
        presentViewController(add, animated: true, completion: nil)
    }
    
    func done(e:Equipment) {
        State.addEquipment(e)
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    func cancel() {
        dismissViewControllerAnimated(true, completion: nil)
    }
}
