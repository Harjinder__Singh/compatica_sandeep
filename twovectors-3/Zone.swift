//
//  Zone.swift
//  twovectors-3
//
//  Created by Nick Bodnar on 10/1/15.
//  Copyright © 2015 Nick Bodnar. All rights reserved.
//

import Foundation
import CoreData

class Zone: NSManagedObject, Syncable {
        
    func toJSON() -> JSON {
        var j = JSON([:])
        
        j["name"].string = name
        j["description"].string = notes
        j["location"].int64 = location

        if let d = State.getMedia(media)?.data {
            j["base64_zone_pic"].stringValue = d.base64EncodedStringWithOptions(NSDataBase64EncodingOptions(rawValue: 0))
        }

        return j
    }
    
    func compare(server: JSON) -> SyncCompare {
        let s = Storage.getBlankEntityInNoMOC("Zone") as! Zone
        s.loadJSON(server)
        
        let idMatch = id == s.id
        let nameMatch = name == s.name
        let locMatch = location == s.location
        if idMatch || (nameMatch && locMatch) {
            if idMatch && nameMatch && locMatch && notes == s.notes && address == s.address && city == s.city && state == s.state && zip == s.zip && media == s.media && modified == s.modified {
                return .Identical
            }
            
            return .SameObject
        }
        
        return .Different
    }
    
    func loadJSON(j: JSON) {
        modified = j["modified_unix"].doubleValue
        
        name = j["name"].stringValue
        id = j["id"].int64Value
        notes = j["description"].string
        location = j["location"]["id"].int64Value
        
        if j["address"]["street_number"].int64 != nil && j["address"]["street_name"].string != nil {
            address = String(j["address"]["street_number"].int64Value) + " " + j["address"]["street_name"].stringValue
        } else {
            address = nil
        }
        city = j["address"]["city"].string
        state = j["address"]["state"].string
        zip = String(j["address"]["zip_code"].int64 ?? 0) ?? "0"
        media = State.getMedia(j["zone_pic"].stringValue).id
    }
    
    static func newObject() -> Zone {
        return Storage.getBlankEntity("Zone") as! Zone
    }
}
