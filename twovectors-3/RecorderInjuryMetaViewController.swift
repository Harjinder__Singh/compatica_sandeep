//
//  RecorderInjuryMetaViewController.swift
//  twovectors-3
//
//  Created by Nick Bodnar on 12/21/15.
//  Copyright © 2015 Nick Bodnar. All rights reserved.
//

import Foundation
import UIKit

class RecorderInjuryMetaViewController: ObjectCollectionListViewController, RecorderPage {
    lazy var _vcs:[ObjectCollection] = {
        let oc:[ObjectCollection] = [InjuryListViewController.init(), IllnessListViewController.init()]
        
        for o in oc {
            o.setParentList(self)
        }
        
        return oc
    }()

    override var vcs:[ObjectCollection]! {
        get {
            return _vcs
        }
    }
    
    override func tapped(cell: ObjectView) {
        for v in vcs {
            v.deselectAll()
        }
        
        if cell is InjuryView {
            (vcs[0] as! InjuryListViewController).detail(cell as! InjuryView)
        }
        if cell is IllnessView {
            (vcs[1] as! IllnessListViewController).detail(cell as! IllnessView)
        }
    }
    
    func pageStatus() -> RecorderPageStatus {
        if State.currentIncident.getInjuries().count == 0 && State.currentIncident.getIllnesses().count == 0 {
            if State.recorderVC.viewControllers!.count == 0 || State.recorderVC.viewControllers![0] == self {
                return .Unknown
            }

            return .Bad
        }
        
        return .Good
    }
    
    func leavingPage(toPage: Int) -> Bool {
        return true
    }
}
