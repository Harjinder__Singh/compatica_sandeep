//
//  Injury.swift
//  twovectors-3
//
//  Created by Nick Bodnar on 10/1/15.
//  Copyright © 2015 Nick Bodnar. All rights reserved.
//

import Foundation
import CoreData

class Injury: NSManagedObject {
    
    func getMajorBodyPartName() -> String {
        return State.getBodyPart(bodyPart)!.major!
    }
    
    func getMinorBodyPartName() -> String {
        return State.getBodyPart(bodyPart)!.minor!
    }
    
    static func newObject() -> Injury {
        return Storage.getBlankEntity("Injury") as! Injury
    }
}
