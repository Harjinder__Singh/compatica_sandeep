//
//  Employee.swift
//  twovectors-3
//
//  Created by Nick Bodnar on 10/1/15.
//  Copyright © 2015 Nick Bodnar. All rights reserved.
//

import Foundation
import CoreData

class Employee: NSManagedObject, Syncable {
    
    func getLastName() -> String {        
        if let space = name!.characters.indexOf(Character(" ")) {
            return name!.substringFromIndex(space.advancedBy(1))
        } else {
            return "lastname"
        }
    }
    
    func toJSON() -> JSON {
        var j = JSON([:])
        
        j["first_name"].string = name!
        j["last_name"].string = "lastname"
        
        if let space = name!.characters.indexOf(Character(" ")) {
            j["first_name"].string = name!.substringToIndex(space)
            j["last_name"].string = name!.substringFromIndex(space.advancedBy(1))
        }
        
        j["employee_id"].string = employeeId
        j["job_title"].string = jobTitle
        j["pay_rate"].double = payRate
        j["role_type"].int32 = roleType > 0 ? roleType : 1
        j["address"].string = address
        j["city"].string = city
        j["state"].string = state
        j["zip_code"].int64Value = Int64(zip ?? "0") ?? 0
        j["contact_name"].string = contactName
        j["contact_relationship"].int64 = contactRelationship > 0 ? contactRelationship : 1
        j["contact_primary_phone"].string = contactPhone
        j["contact_email"].string = contactEmail
        j["company"].int64 = Util.getCompany()
        j["marital_status"].int64 = maritalStatus > 0 ? maritalStatus : 1
        if let d = State.getMedia(media)?.data {
            j["base64_employee_pic"].stringValue = d.base64EncodedStringWithOptions(NSDataBase64EncodingOptions(rawValue: 0))
        }
        
        return j
    }
    
    func compare(server: JSON) -> SyncCompare {
        let s = Storage.getBlankEntityInNoMOC("Employee") as! Employee
        s.loadJSON(server)
        
        let idMatch = id == s.id
        let empIdMatch = employeeId == s.employeeId
        let nameMatch = name == s.name
        if idMatch || (nameMatch && empIdMatch) {
            if idMatch && nameMatch && empIdMatch && jobTitle == s.jobTitle && payRate == s.payRate && roleType == s.roleType && address == s.address && city == s.city && state == s.state && zip == s.zip && contactName == s.contactName && contactRelationship == s.contactRelationship && contactPhone == s.contactPhone && contactEmail == s.contactEmail && maritalStatus == s.maritalStatus && companyId == s.companyId && media == s.media && username == s.username && modified == s.modified {
                return .Identical
            }
            
            return .SameObject
        }

        return .Different
    }
    
    func loadJSON(j: JSON) {
        modified = j["modified_unix"].doubleValue
        
        name = j["first_name"].stringValue + " " + j["last_name"].stringValue
        id = j["id"].int64Value
        
        employeeId = j["employee_id"].string
        jobTitle = j["job_title"].string
        payRate = j["pay_rate"].double ?? 0
        roleType = j["role_type"].int32 ?? 1
        
        if j["address"]["street_number"].int64 != nil && j["address"]["street_name"].string != nil {
            address = String(j["address"]["street_number"].int64Value) + " " + j["address"]["street_name"].stringValue
        } else {
            address = nil
        }
        city = j["address"]["city"].string
        state = j["address"]["state"].string
        zip = String(j["address"]["zip_code"].int64 ?? 0) ?? "0"
        
        if j["contacts"][0]["first_name"].string != nil && j["contacts"][0]["last_name"].string != nil {
            contactName = j["contacts"][0]["first_name"].stringValue + " " + j["contacts"][0]["last_name"].stringValue
        } else {
            contactName = nil
        }
        contactRelationship = j["contacts"][0]["relationship"].int64 ?? 1
        contactPhone = j["contacts"][0]["primary_phone"]["phone"].string
        contactEmail = j["contacts"][0]["email"]["email"].string
        maritalStatus = j["marital_status"].int64 ?? 1
        username = j["user"]["username"].string
        companyId = j["company"]["id"].int64Value
        media = State.getMedia(j["employee_pic"].stringValue).id
    }
    
    static func newObject() -> Employee {
        return Storage.getBlankEntity("Employee") as! Employee
    }
}
