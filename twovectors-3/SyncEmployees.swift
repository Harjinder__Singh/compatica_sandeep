//
//  SyncEmployees.swift
//  compatica
//
//  Created by Nick Bodnar on 1/24/16.
//  Copyright © 2016 Nick Bodnar. All rights reserved.
//

import Foundation

class SyncEmployees: Sync<Employee> {
    
    override func getServerList(callback: ((ErrorType?, [JSON]?) -> Void)) {
        webRequest("employee/?json&include=address,contacts", nil, "GET") {
            (err, j) in
            if err != nil {
                callback(err, nil)
            } else {
                // TODO: do this better
                for e in State.getEmployees() {
                    for ee in JSON(data: j!).arrayValue {
                        if e.companyId != ee["company"]["id"].int64Value {
                            abort()
                        }
                    }
                }
                callback(err, JSON(data: j!).array)
            }
        }
    }
    
    override func getClientList() -> [Employee] {
        return State.getEmployees()
    }
    
    override func serverNew(obj: JSON) {
        let o = Employee.newObject()
        o.loadJSON(obj)
        
        updatePic(o)
        
        State.addEmployee(o)
    }

    override func clientNew(obj: Employee) {
        webRequest("employee/create/?json", obj, "POST") {
            error, data in
            // TODO: handle error
            print(JSON(data: data!))
        }
    }

    override func update(server: JSON, _ client: Employee) {
        if client.modified < server["modified_unix"].doubleValue {
            let oldId = client.id
            client.loadJSON(server)
            
            updatePic(client)
            
            if server["id"].int64Value != oldId {
                for i in State.getIncidents() {
                    if i.employee == oldId {
                        i.employee = server["id"].int64Value
                    }
                }
            }
            
            Storage.save()
        } else {
            webRequest("employee/" + String(client.id) + "/update/?json", client, "POST") {
                error, data in
                if error != nil {
                    // TODO: handle this
                }
                
                //self.afterPost(client.id, client)
            }
        }
    }
    
    func updatePic(o:Employee) {
        if let m = State.getMedia(o.media) {
            if m.data != nil {
                return
            } else {
                webRequest("media/" + m.url!, nil, "GET") {
                    (err, data) in
                    m.data = data
                    Storage.save()
                }
            }
        }
    }
    
    func afterPost(id: Int64, _ client:Employee) {
        webRequest("employee/" + String(id) + "/?json", nil, "GET") {
            (err, j) in
            if err != nil {
                // TODO: handle this
            } else {
                self.update(JSON(data: j!).arrayValue[0], client)
            }
        }
    }

    override func done(success:Bool) {
        if success {
            SyncLocations().sync()
        }
    }
}
