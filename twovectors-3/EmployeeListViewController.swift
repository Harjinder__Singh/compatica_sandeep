//
//  RecorderEmployeeViewController2.swift
//  twovectors-3
//
//  Created by Nick Bodnar on 9/11/15.
//  Copyright (c) 2015 Nick Bodnar. All rights reserved.
//

import Foundation
import UIKit

class EmployeeListViewController: ObjectCollectionViewController<EmployeeView>, AddEmployeeViewControllerDelegate {
    var mainSelector = true
    
    required override init() {
        super.init()
        searchBarHidden = false
        addButtonHidden = false
        headerText = "Select Employee"
    }
    
    convenience init(main:Bool) {
        self.init()
        mainSelector = main
        if !main {
            itemSize = ObjectViewSizes.smallSize
        }
    }
    
    override func setupDataCallbacks() {
        State.setOnEmployeesUpdated(update)
    }
    
    override func updateData() -> [objectType] {
        let employees = State.getEmployees()

        var result = [EmployeeView]()
        let now = NSDate.timeIntervalSinceReferenceDate()
        for e in employees {
            if filter == "" || e.name!.lowercaseString.containsString(filter.lowercaseString) {
                let em = EmployeeView(frame: CGRectMake(0, 0, 150, 75))
                em.employee = e
                if mainSelector && State.currentIncident?.employee == e.id {
                    em.objectSelected = true
                }
                result.append(em)
            }
        }
        
        let after1 = NSDate.timeIntervalSinceReferenceDate()
        
        result.sortInPlace() {
            (a, b) in
            return a.employee.getLastName().lowercaseString < b.employee.getLastName().lowercaseString
        }
        
        let after2 = NSDate.timeIntervalSinceReferenceDate()
        
        print(after1 - now)
        print(after2 - after1)
        
        return result
    }
    
    override func detail(cell: objectType) {
        let add = AddEmployeeViewController()
        add.type = .Edit
        add.employee = cell.employee
        add.delegate = self
        presentViewController(add, animated: true, completion: nil)
    }
    
    override func add() {
        let add = AddEmployeeViewController()
        add.type = .Create
        add.delegate = self
        presentViewController(add, animated: true, completion: nil)
    }
    
    func done(e:Employee) {
        State.addEmployee(e)
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    func cancel() {
        dismissViewControllerAnimated(true, completion: nil)
    }
}
