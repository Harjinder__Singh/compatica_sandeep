//
//  ER.swift
//  twovectors-3
//
//  Created by Nick Bodnar on 10/1/15.
//  Copyright © 2015 Nick Bodnar. All rights reserved.
//

import Foundation
import CoreData

class ER: NSManagedObject, Syncable {
        
    func toJSON() -> JSON {
        var j = JSON([:])
        
        j["name"].string = name!
        
        return j
    }
    
    func compare(server: JSON) -> SyncCompare {
        let s = Storage.getBlankEntityInNoMOC("ER") as! ER
        s.loadJSON(server)
        
        let idMatch = id == s.id
        if idMatch {
            if idMatch && name == s.name && notes == s.notes && address == s.address && city == s.city && state == s.state && zip == s.zip && modified == s.modified {
                return .Identical
            }
            
            return .SameObject
        }
        
        return .Different
    }
    
    func loadJSON(j: JSON) {
        modified = j["modified_unix"].doubleValue
        
        name = j["name"].stringValue
        id = j["id"].int64Value
        notes = j["description"].string
        
        if j["address"]["street_number"].int64 != nil && j["address"]["street_name"].string != nil {
            address = String(j["address"]["street_number"].int64Value) + " " + j["address"]["street_name"].stringValue
        } else {
            address = nil
        }
        city = j["address"]["city"].string
        state = j["address"]["state"].string
        zip = String(j["address"]["zip_code"].int64 ?? 0) ?? "0"
        lat = j["address"]["latitude"].double ?? 0
        lon = j["address"]["longitude"].double ?? 0
    }
    
    static func newObject() -> ER {
        return Storage.getBlankEntity("ER") as! ER
    }
}
