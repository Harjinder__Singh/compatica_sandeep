//
//  IllnessTypeSelectorViewController.swift
//  compatica
//
//  Created by Nick Bodnar on 1/25/16.
//  Copyright © 2016 Nick Bodnar. All rights reserved.
//

import Foundation
import UIKit

class IllnessTypeSelectorViewController: ObjectCollectionListViewController {
    var delegate:IllnessTypeSelectorViewControllerDelegate!
    
    lazy var _vcs:[ObjectCollection] = {
        let oc:[ObjectCollection] = [IllnessTypeListViewController()]
        
        for o in oc {
            o.setParentList(self)
        }
        
        return oc
    }()
    
    override var vcs:[ObjectCollection]! {
        get {
            return _vcs
        }
    }
    
    override func tapped(cell: ObjectView) {
        let i = cell as! IllnessTypeView
        delegate.done(i.illnessType)
    }
}
