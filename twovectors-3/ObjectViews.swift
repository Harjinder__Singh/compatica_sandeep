//
//  EmployeeView.swift
//  twovectors-3
//
//  Created by Nick Bodnar on 10/20/15.
//  Copyright © 2015 Nick Bodnar. All rights reserved.
//

import Foundation
import UIKit

class ObjectView: XibViewBase {
    override var layer:CALayer {
        let l = super.layer
        l.borderWidth=1.0
        l.cornerRadius = 5
        l.masksToBounds = true
        l.borderColor=UIColor.blackColor().CGColor
        return l
    }
    
    var defaultColor = UIColor(red: 59/256, green: 98/256, blue: 173/256, alpha: 1)
    var selectedColor = UIColor.greenColor()
    
    var _defaultImage = "no-image.gif"
    
    var defaultImage:String {
        get {
            return _defaultImage
        }
        set {
            image.image = UIImage(named: newValue)
            _defaultImage = newValue
        }
    }
    
    var _objectSelected = false
    var objectSelected:Bool {
        get {
            return _objectSelected
        }
        set {
            _objectSelected = newValue
            bodyView.backgroundColor = _objectSelected ? selectedColor : defaultColor
        }
    }
    
    func hideImage() {
        image.hidden = true
        imageToLabels.constant = -1 * image.frame.width
        label.font = label.font.fontWithSize(12)
        label.textAlignment = .Center
    }
    
    func hideDetails() {
        detailView.hidden = true
        mainViewHeight.constant = mainViewHeight.constant + detailView.frame.height + 1
    }
    
    func showSecondLabel() {
        secondLabelHeight.constant = bodyView.frame.height * 0.3
    }

    @IBOutlet weak var bodyView: UIView!
    @IBOutlet weak var image: UIImageView!
    @IBOutlet weak var label: UILabel!
    @IBOutlet weak var detail: UIButton!
    @IBOutlet weak var detailView: UIView!
    @IBOutlet weak var secondLabel: UILabel!
    
    @IBOutlet weak var secondLabelHeight: NSLayoutConstraint!
    @IBOutlet var imageToLabels: NSLayoutConstraint!
    @IBOutlet var mainViewHeight: NSLayoutConstraint!
}

class IncidentView: ObjectView {
    var _incident:Incident!
    
    var incident:Incident {
        get {
            return _incident
        }
        set {
            _incident = newValue
            label.text = newValue.getEmployee().name! + "\n" + Util.dateFormatter.stringFromDate(NSDate(timeIntervalSinceReferenceDate: newValue.time))
            image.image = Util.getImage(newValue.getEmployee().media, "no-image.gif")
        }
    }
    
    override func customizeLayout() {
        hideDetails()
    }
}

class EmployeeView: ObjectView {
    var _employee:Employee!
    
    var employee:Employee {
        get {
            return _employee
        }
        set {
            _employee = newValue
            
            label.text = newValue.name
            if let space = newValue.name?.characters.indexOf(Character(" ")) {
                label.text = newValue.name!.substringToIndex(space) + "\n" + newValue.name!.substringFromIndex(space.advancedBy(1))
            }
            
            image.image = Util.getImage(newValue.media, "no-image.gif")
        }
    }
}

class LocationView: ObjectView {
    var _location:Location!
    
    var location:Location {
        get {
            return _location
        }
        set {
            _location = newValue
            label.text = newValue.name
            image.image = Util.getImage(newValue.media, "location.png")
        }
    }
}

class ZoneView: ObjectView {
    var _zone:Zone!
    
    var zonex:Zone {
        get {
            return _zone
        }
        set {
            _zone = newValue
            label.text = newValue.name
            image.image = Util.getImage(newValue.media, "location2.png")
        }
    }
}

class EquipmentView: ObjectView{
    var _equipment:Equipment!
    
    var equipment:Equipment {
        get {
            return _equipment
        }
        set {
            _equipment = newValue
            label.text = newValue.name
            image.image = Util.getImage(newValue.media, "equipment.png")
        }
    }
}

class InjuryView: ObjectView {
    var _injury:Injury!

    var injury:Injury {
        get {
            return _injury
        }
        set {
            _injury = newValue
            label.text = newValue.getMajorBodyPartName() + "\n" + Util.severityToText(newValue.severity)
            image.image = Util.getImage(newValue.media, "injury.png")
        }
    }
    
    override func customizeLayout() {
        hideDetails()
    }
}

class IllnessView: ObjectView {
    var _illness:Illness!
    
    var illness:Illness {
        get {
            return _illness
        }
        set {
            _illness = newValue
            label.text = newValue.type! + "\n" + Util.severityToText(newValue.severity)
            image.image = Util.getImage(newValue.media, "illness.png")
        }
    }
    
    override func customizeLayout() {
        hideDetails()
    }
}

class MajorBodyPartView: ObjectView {
    var _bodyPart:BodyPart!
    
    var bodyPart:BodyPart {
        get {
            return _bodyPart
        }
        set {
            _bodyPart = newValue
            label.text = newValue.major!
        }
    }
    
    override func customizeLayout() {
        hideImage()
        hideDetails()
    }
}

class MinorBodyPartView: ObjectView {
    var _bodyPart:BodyPart!
    
    var bodyPart:BodyPart {
        get {
            return _bodyPart
        }
        set {
            _bodyPart = newValue
            label.text = newValue.minor!
        }
    }
    
    override func customizeLayout() {
        hideImage()
        hideDetails()
    }
}

class InjuryTypeView: ObjectView {
    var _injuryType:String!
    
    var injuryType:String {
        get {
            return _injuryType
        }
        set {
            _injuryType = newValue
            label.text = newValue
        }
    }
    
    override func customizeLayout() {
        hideImage()
        hideDetails()
    }
}

class IllnessTypeView: ObjectView {
    var _illnessType:String!
    
    var illnessType:String {
        get {
            return _illnessType
        }
        set {
            _illnessType = newValue
            label.text = newValue
        }
    }
    
    override func customizeLayout() {
        hideImage()
        hideDetails()
    }
}

class FirstAidResponseView: ObjectView {
    var _answer:Answer!
    
    var answer:Answer {
        get {
            return _answer
        }
        set {
            _answer = newValue
            label.text = newValue.rawValue
        }
    }
    
    override func customizeLayout() {
        hideImage()
    }
}

class MedicalAttentionResponseView: ObjectView {
    var _answer:Answer!
    
    var answer:Answer {
        get {
            return _answer
        }
        set {
            _answer = newValue
            label.text = newValue.rawValue
        }
    }
    
    override func customizeLayout() {
        hideImage()
    }
}

class ResponseDestinationView: ObjectView {
    var _dest:Destination!
    
    var destination:Destination {
        get {
            return _dest
        }
        set {
            _dest = newValue
            label.text = newValue.rawValue
        }
    }
    
    override func customizeLayout() {
        hideImage()
        hideDetails()
    }
}

class ClinicView: ObjectView {
    var _clinic:Clinic!
    
    var clinic:Clinic {
        get {
            return _clinic
        }
        set {
            _clinic = newValue
            let distance = Util.clinicDistance(newValue)
            
            label.text = newValue.name!
            secondLabel.text = ((distance >= 0) ? String(distance) : "Unknown") + " Miles"
        }
    }
    
    override func customizeLayout() {
        hideImage()
        showSecondLabel()
    }
}

class ERView: ObjectView {
    var _er:ER!
    
    var er:ER {
        get {
            return _er
        }
        set {
            _er = newValue
            
            let distance = Util.erDistance(newValue)
            
            label.text = newValue.name!
            secondLabel.text = ((distance >= 0) ? String(distance) : "Unknown") + " Miles"
        }
    }
    
    override func customizeLayout() {
        hideImage()
        showSecondLabel()
    }
}

class StatementView: ObjectView {
    var _statement:Statement!
    
    var statement:Statement {
        get {
            return _statement
        }
        set {
            _statement = newValue
            
            // remove
            newValue.madeByDesciptor = "Witness"
            
            label.text = newValue.getMediaType() + "\n" + newValue.getMadeByName() + " (" + newValue.madeByDesciptor! + ")"
            image.image = Util.getImage(newValue.media, "no-image.gif")
        }
    }
    
    override func customizeLayout() {
        hideDetails()
    }
}
