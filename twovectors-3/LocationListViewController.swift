//
//  RecorderLocationViewController.swift
//  twovectors-3
//
//  Created by Nick Bodnar on 9/8/15.
//  Copyright (c) 2015 Nick Bodnar. All rights reserved.
//

import Foundation
import UIKit

class LocationListViewController: ObjectCollectionViewController<LocationView>, AddLocationViewControllerDelegate {
    
    required override init() {
        super.init()
        searchBarHidden = false
        addButtonHidden = false
        headerText = "Select Location"
    }
    
    override func setupDataCallbacks() {
        State.setOnLocationsUpdated(update)
    }
    
    override func updateData() -> [objectType] {
        let locations = State.getLocations()
        
        var result = [LocationView]()
        for l in locations {
            if filter == "" || l.name!.lowercaseString.containsString(filter.lowercaseString) {
                let lv = LocationView(frame: CGRectMake(0, 0, 150, 75))
                lv.location = l
                if State.currentIncident.location == l.id {
                    lv.objectSelected = true
                    result = [LocationView]()
                    result.append(lv)
                    return result
                }
                result.append(lv)
            }
        }
        
        result.sortInPlace() {
            (a, b) in
            return a.location.name?.lowercaseString < b.location.name?.lowercaseString
        }
        
        return result
    }
    
    override func detail(cell: objectType) {
        let add = AddLocationViewController()
        add.type = .Edit
        add.location = cell.location
        add.delegate = self
        presentViewController(add, animated: true, completion: nil)
    }
    
    override func add() {
        let add = AddLocationViewController()
        add.type = .Create
        add.delegate = self
        presentViewController(add, animated: true, completion: nil)
    }

    func done(l:Location) {
        State.addLocation(l)
        dismissViewControllerAnimated(true, completion: nil)
    }

    func cancel() {
        dismissViewControllerAnimated(true, completion: nil)
    }
}

