//
//  RecorderInjuryViewController.swift
//  twovectors-3
//
//  Created by Nick Bodnar on 10/23/15.
//  Copyright © 2015 Nick Bodnar. All rights reserved.
//

import Foundation
import UIKit

class InjuryListViewController: ObjectCollectionViewController<InjuryView>, AddInjuryViewControllerDelegate {
    
    required override init() {
        super.init()
        detailBar = false
        addButtonHidden = false
        headerText = "Injury List"
    }

    override func setupDataCallbacks() {
        State.setOnInjuriesUpdated(update)
    }
    
    override func updateData() -> [objectType] {
        let injuries = State.currentIncident.getInjuries()
        
        var result = [InjuryView]()
        for i in injuries {
            let iv = InjuryView(frame: CGRectMake(0, 0, 150, 75))
            iv.injury = i
            result.append(iv)
        }
        
        return result
    }
    
    override func detail(cell: objectType) {
        let add = AddInjuryViewController()
        add.type = .Edit
        add.injury = cell.injury
        add.delegate = self
        presentViewController(add, animated: true, completion: nil)
    }
    
    override func add() {
        let add = AddInjuryViewController()
        add.type = .Create
        add.delegate = self
        presentViewController(add, animated: true, completion: nil)
    }
    
    func done(i:Injury) {
        State.addInjury(i)
        
        cancel()
        
        added()
    }
    
    func cancel() {
        dismissViewControllerAnimated(true, completion: nil)
    }
}
