//
//  MedicalAttentionResponseListViewController
//  twovectors-3
//
//  Created by Nick Bodnar on 12/21/15.
//  Copyright © 2015 Nick Bodnar. All rights reserved.
//

import Foundation
import UIKit

class MedicalAttentionResponseListViewController: ObjectCollectionViewController<MedicalAttentionResponseView>, AddResponseDetailViewControllerDelegate {
    
    required override init() {
        super.init()
        headerText = "Is Medical Attention Required?"
    }
    
    override func setupDataCallbacks() {
    }
    
    override func updateData() -> [objectType] {
        let responses = [Answer.Yes, Answer.No, Answer.Refused]
        
        var result = [MedicalAttentionResponseView]()
        for r in responses {
            let rv = MedicalAttentionResponseView(frame: CGRectMake(0, 0, 150, 75))
            rv.answer = r
            if State.currentIncident.medicalAttentionRequired && r == .Yes {
                rv.objectSelected = true
                result = [MedicalAttentionResponseView]()
                result.append(rv)
                return result
            }
            if State.currentIncident.medicalAttentionNotRequired && r == .No {
                rv.objectSelected = true
                result = [MedicalAttentionResponseView]()
                result.append(rv)
                return result
            }
            if State.currentIncident.medicalAttentionRefused && r == .Refused {
                rv.objectSelected = true
                result = [MedicalAttentionResponseView]()
                result.append(rv)
                return result
            }
            result.append(rv)
        }
        
        return result
    }
    
    override func detail(cell: objectType) {
        let vc = AddResponseDetailViewController()
        vc.delegate = self
        presentViewController(vc, animated: true, completion: nil)
    }
    
    func done(vc: AddResponseDetailViewController) {
        cancel()
    }
    
    func cancel() {
        dismissViewControllerAnimated(true, completion: nil)
    }
}