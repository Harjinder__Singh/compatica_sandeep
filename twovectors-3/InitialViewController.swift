//
//  InitialViewController.swift
//  twovectors-3
//
//  Created by Nick Bodnar on 10/15/15.
//  Copyright © 2015 Nick Bodnar. All rights reserved.
//

import Foundation

import UIKit

class InitialViewController:UISplitViewController, UISplitViewControllerDelegate {
//    override var delegate:UISplitViewControllerDelegate? {
//        get {
//            return self
//        }
//        set {
//            
//        }
//    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        delegate = self
    }
    
    override func viewWillTransitionToSize(size: CGSize, withTransitionCoordinator coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransitionToSize(size, withTransitionCoordinator: coordinator)
    }
    
    override func willTransitionToTraitCollection(newCollection: UITraitCollection, withTransitionCoordinator coordinator: UIViewControllerTransitionCoordinator) {
        super.willTransitionToTraitCollection(newCollection, withTransitionCoordinator: coordinator)
    }
    
    override func showViewController(vc: UIViewController, sender: AnyObject?) {
        super.showViewController(vc, sender: sender)
    }
    
    override func showDetailViewController(vc: UIViewController, sender: AnyObject?) {
        super.showDetailViewController(vc, sender: sender)
    }
    
    // This method allows a client to update any bar button items etc.
    func splitViewController(svc: UISplitViewController, willChangeToDisplayMode displayMode: UISplitViewControllerDisplayMode) {
        
    }
    
    // Called by the gesture AND barButtonItem to determine what they will set the display mode to (and what the displayModeButtonItem's appearance will be.) Return UISplitViewControllerDisplayModeAutomatic to get the default behavior.
    func targetDisplayModeForActionInSplitViewController(svc: UISplitViewController) -> UISplitViewControllerDisplayMode {
        return UISplitViewControllerDisplayMode.Automatic
    }
    
    // Override this method to customize the behavior of `showViewController:` on a split view controller. Return YES to indicate that you've handled
    // the action yourself; return NO to cause the default behavior to be executed.
    func splitViewController(splitViewController: UISplitViewController, showViewController vc: UIViewController, sender: AnyObject?) -> Bool {
        return false
    }
    
    // Override this method to customize the behavior of `showDetailViewController:` on a split view controller. Return YES to indicate that you've
    // handled the action yourself; return NO to cause the default behavior to be executed.
    func splitViewController(splitViewController: UISplitViewController, showDetailViewController vc: UIViewController, sender: AnyObject?) -> Bool {
        return false
    }
    
    // Return the view controller which is to become the primary view controller after `splitViewController` is collapsed due to a transition to
    // the horizontally-compact size class. If you return `nil`, then the argument will perform its default behavior (i.e. to use its current primary view
    // controller).
    func primaryViewControllerForCollapsingSplitViewController(splitViewController: UISplitViewController) -> UIViewController? {
        return viewControllers[0]
    }
    
    // Return the view controller which is to become the primary view controller after the `splitViewController` is expanded due to a transition
    // to the horizontally-regular size class. If you return `nil`, then the argument will perform its default behavior (i.e. to use its current
    // primary view controller.)
    func primaryViewControllerForExpandingSplitViewController(splitViewController: UISplitViewController) -> UIViewController? {
        return nil
    }
    
    // This method is called when a split view controller is collapsing its children for a transition to a compact-width size class. Override this
    // method to perform custom adjustments to the view controller hierarchy of the target controller.  When you return from this method, you're
    // expected to have modified the `primaryViewController` so as to be suitable for display in a compact-width split view controller, potentially
    // using `secondaryViewController` to do so.  Return YES to prevent UIKit from applying its default behavior; return NO to request that UIKit
    // perform its default collapsing behavior.
    func splitViewController(splitViewController: UISplitViewController, collapseSecondaryViewController secondaryViewController: UIViewController, ontoPrimaryViewController primaryViewController: UIViewController) -> Bool {
        return false
    }
    
    // This method is called when a split view controller is separating its child into two children for a transition from a compact-width size
    // class to a regular-width size class. Override this method to perform custom separation behavior.  The controller returned from this method
    // will be set as the secondary view controller of the split view controller.  When you return from this method, `primaryViewController` should
    // have been configured for display in a regular-width split view controller. If you return `nil`, then `UISplitViewController` will perform
    // its default behavior.
    func splitViewController(splitViewController: UISplitViewController, separateSecondaryViewControllerFromPrimaryViewController primaryViewController: UIViewController) -> UIViewController? {
        return nil
    }
    
    func splitViewControllerSupportedInterfaceOrientations(splitViewController: UISplitViewController) -> UIInterfaceOrientationMask {
        return UIInterfaceOrientationMask.All
    }
    
    func splitViewControllerPreferredInterfaceOrientationForPresentation(splitViewController: UISplitViewController) -> UIInterfaceOrientation {
        return UIInterfaceOrientation.LandscapeLeft
    }
}