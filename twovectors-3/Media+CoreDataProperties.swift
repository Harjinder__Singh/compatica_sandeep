//
//  Media+CoreDataProperties.swift
//  compatica
//
//  Created by Nick Bodnar on 3/14/16.
//  Copyright © 2016 Nick Bodnar. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension Media {

    @NSManaged var data: NSData?
    @NSManaged var id: Int64
    @NSManaged var url: String?
    @NSManaged var type: Int16

}
