//
//  RecorderSummaryViewController.swift
//  twovectors-3
//
//  Created by Nick Bodnar on 9/8/15.
//  Copyright (c) 2015 Nick Bodnar. All rights reserved.
//

import Foundation
import UIKit

class RecorderSummaryViewController: DetailViewController, RecorderPage {
    
    var employee = UIButton()
    var time = UIButton()
    var location = UIButton()
    var zonex = UIButton()
    var equipment = UIButton()
    var injuries = [UIButton]()
    var illnesses = [UIButton]()
    var firstAid = UIButton()
    var medicalAttention = UIButton()
    var destination = UIButton()
    var escort = UIButton()
    var destinationName = UIButton()
    var statements = [UIButton]()
    
    var submitButton = UIButton()
    
    override func loadView() {
        super.loadView()
        
        scrollY = 0.0
        scrollHeightDiff = 50.0
        
        doneButton.hidden = true
        cancelButton.hidden = true
    }

    override func viewWillAppear(animated: Bool) {
        employee.setTitle(State.currentIncident.getEmployee().name, forState: .Normal)
        location.setTitle(State.currentIncident.getLocation()?.name, forState: .Normal)
        zonex.setTitle(State.currentIncident.getZone()?.name, forState: .Normal)
        equipment.setTitle(State.currentIncident.getEquipment()?.name, forState: .Normal)
        
        injuries = [UIButton]()
        for i in State.currentIncident.getInjuries() {
            let b = UIButton()
            b.setTitle(i.getMajorBodyPartName() + "-" + i.getMinorBodyPartName(), forState: .Normal)
            injuries.append(b)
        }
        
        illnesses = [UIButton]()
        for i in State.currentIncident.getIllnesses() {
            let b = UIButton()
            b.setTitle(i.type, forState: .Normal)
            illnesses.append(b)
        }
        
        firstAid.setTitle(State.currentIncident.firstAidGiven ? "Yes" : "No", forState: .Normal)
        medicalAttention.setTitle(State.currentIncident.medicalAttentionRequired ? "Yes" : State.currentIncident.medicalAttentionRefused ? "Refused" : "No", forState: .Normal)
        
        if State.currentIncident.destinationHome {
            destination.setTitle("Home", forState: .Normal)
            destinationName.setTitle("Home", forState: .Normal)
        } else if State.currentIncident.destinationWork {
            destination.setTitle("Work", forState: .Normal)
            destinationName.setTitle("Work", forState: .Normal)
        } else if State.currentIncident.destinationClinic {
            destination.setTitle("Clinic", forState: .Normal)
            destinationName.setTitle(State.getClinic(State.currentIncident.destinationClinicId)?.name ?? "Unknown", forState: .Normal)
        } else if State.currentIncident.destinationER {
            destination.setTitle("Hospital", forState: .Normal)
            destinationName.setTitle(State.getClinic(State.currentIncident.destinationERId)?.name ?? "Unknown", forState: .Normal)
        }
        
        escort.setTitle(State.getEmployee(State.currentIncident.destinationEscortedBy)?.name ?? "n/a", forState: .Normal)

        statements = [UIButton]()
        for i in State.currentIncident.getStatements() {
            let b = UIButton()
            b.setTitle(i.getMadeByName(), forState: .Normal)
            statements.append(b)
        }
        
        submitButton.setTitle("Submit", forState: .Normal)
        
        clear()
        
        addButtonDetail(employee, "Name", "view0:")

        addTimeDetail(time, "Time")
        time.setTitle(Util.dateTimeFormatter.stringFromDate(NSDate(timeIntervalSinceReferenceDate: State.currentIncident.time)), forState: .Normal)
        
        addButtonDetail(location, "Location", "view1:")
        addButtonDetail(zonex, "Zone", "view1:")
        addButtonDetail(equipment, "Equipment", "view1:")
        
        for b in injuries {
            addButtonDetail(b, "Injury", "view2:")
        }
        for b in illnesses {
            addButtonDetail(b, "Illness", "view2:")
        }
        
        addButtonDetail(firstAid, "First Aid Given", "view3:")
        addButtonDetail(medicalAttention, "Medical Attention", "view3:")
        addButtonDetail(destination, "Destination", "view3:")
        addButtonDetail(escort, "Escorted By", "view3:")
        addButtonDetail(destinationName, "Clinic/Hospital", "view3:")
        
        for b in statements {
            addButtonDetail(b, "Statement", "view4:")
        }
        
        addSpace()
        addSpace()
        
        addButtonDetail(submitButton, "Tap To Submit", "submit:")
        
        super.viewWillAppear(animated)
    }
    
    override func dateChanged(datePicker: UIDatePicker) {
        super.dateChanged(datePicker)
        State.currentIncident.time = datePicker.date.timeIntervalSinceReferenceDate
    }
    
    func submit(sender: AnyObject) {
        State.submit()
        
        let alert = UIAlertController(title: "Submitted!", message: "The incident is being uploaded now, or it will be once internet is available.", preferredStyle: .Alert)
        
        let okAction = UIAlertAction(title: "OK", style: .Default) {
            a in
            State.recorder.exit(UITapGestureRecognizer())
        }
        alert.addAction(okAction)
        
        presentViewController(alert, animated: true, completion: nil)
    }
    
    func view0(sender: UITapGestureRecognizer) {
        State.recorderVC.goToPage(0)
    }
    func view1(sender: UITapGestureRecognizer) {
        State.recorderVC.goToPage(1)
    }
    func view2(sender: UITapGestureRecognizer) {
        State.recorderVC.goToPage(2)
    }
    func view3(sender: UITapGestureRecognizer) {
        State.recorderVC.goToPage(3)
    }
    func view4(sender: UITapGestureRecognizer) {
        State.recorderVC.goToPage(4)
    }
    
    func pageStatus() -> RecorderPageStatus {
        return .Good
    }
    
    func leavingPage(toPage: Int) -> Bool {
        return true
    }
}
