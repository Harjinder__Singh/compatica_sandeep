//
//  ObjectCollectionHeader.swift
//  twovectors-3
//
//  Created by Nick Bodnar on 12/24/15.
//  Copyright © 2015 Nick Bodnar. All rights reserved.
//

import Foundation
import UIKit

class ObjectCollectionHeader:XibViewBase {
    @IBOutlet weak var label: UILabel!
    @IBOutlet weak var addButton: UIButton!
    @IBOutlet weak var searchBar: UISearchBar!

    @IBOutlet weak var searchWidth: NSLayoutConstraint!
    @IBOutlet weak var addButtonWidth: NSLayoutConstraint!
}