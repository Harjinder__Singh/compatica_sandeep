//
//  NavigationIconView.swift
//  twovectors-3
//
//  Created by Nick Bodnar on 10/24/15.
//  Copyright © 2015 Nick Bodnar. All rights reserved.
//

import Foundation
import UIKit

@IBDesignable class NavigationIconView: XibViewBase {
    var _index:Int = 0
    @IBOutlet weak var image: UIImageView!
    @IBOutlet weak var statusImage: UIImageView!
    @IBOutlet weak var text: UILabel!
    
    @IBInspectable var index:Int {
        get {
            return _index
        }
        set {
            _index = newValue
        }
    }
    
    @IBInspectable var imageIcon:UIImage? {
        get {
            return image.image
        }
        set {
            image.image = newValue
        }
    }
    
    @IBInspectable var labelText:String? {
        get {
            return text.text
        }
        set {
            text.text = newValue
        }
    }
    
    @IBAction func tapped(sender: UITapGestureRecognizer) {
        State.recorderVC.goToPage(index)
    }
}