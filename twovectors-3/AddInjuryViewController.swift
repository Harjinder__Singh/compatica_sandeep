//
//  AddInjuryViewController.swift
//  twovectors-3
//
//  Created by Nick Bodnar on 12/31/15.
//  Copyright © 2015 Nick Bodnar. All rights reserved.
//

import Foundation
import UIKit

class AddInjuryViewController: DetailViewController, BodyPartSelectorViewControllerDelegate, InjuryTypeSelectorViewControllerDelegate {
    var delegate:AddInjuryViewControllerDelegate!

    var injury:Injury!
    var bodyPartId:Int64 = 0
    
    var image = UIImageView(image: UIImage(named: "injury.png"))
    var bodyPart = UIButton()
    var injuryDescription = UITextView()
    var injuryType = UIButton()
    var sev0 = UIButton()
    var sev1 = UIButton()
    var sev2 = UIButton()
    
    var severity:Int16 = 0
    
    override func loadView() {
        super.loadView()
        
        bodyPart.setTitle("Tap to Select", forState: .Normal)
        injuryType.setTitle("Tap to Select", forState: .Normal)
        
        sev0.setTitle("Record Only", forState: .Normal)
        sev1.setTitle("First Aid", forState: .Normal)
        sev2.setTitle("Severe", forState: .Normal)
        
        addImageDetail(image, "Photo\n(Tap to Change)")
        addButtonDetail(bodyPart, "Body Part", "selectBodyPart:")
        addSpace()
        addMultiButtonDetail([sev0, sev1, sev2], ["sev0:", "sev1:", "sev2:"])
        addSpace()
        addButtonDetail(injuryType, "Injury Type", "selectInjuryType:")
        addTextViewDetail(injuryDescription, "Description", true, 150)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if type != .Create {
            bodyPart.setTitle(injury.getMajorBodyPartName() + " - " + injury.getMinorBodyPartName(), forState: .Normal)
            
            if injury.severity == 0 {
                sev0(NSObject())
            }
            if injury.severity == 1 {
                sev1(NSObject())
            }
            if injury.severity == 2 {
                sev2(NSObject())
            }
            
            bodyPartId = injury.bodyPart
            injuryDescription.text = injury.notes
            injuryType.setTitle(injury.type, forState: .Normal)
        } else {
            dispatch_async(dispatch_get_main_queue()) {
                self.selectBodyPartInternal()
            }
        }
        
        colorButtons()
    }
    
    func selectBodyPart(sender: AnyObject) {
        selectBodyPartInternal()
    }
    
    func selectInjuryType(sender: AnyObject) {
        let vc = InjuryTypeSelectorViewController()
        vc.delegate = self
        
        Util.modal(vc, self)
    }
    
    func sev0(sender: AnyObject) {
        severity = 0
        colorButtons()
    }
    
    func sev1(sender: AnyObject) {
        severity = 1
        colorButtons()
    }
    
    func sev2(sender: AnyObject) {
        severity = 2
        colorButtons()
    }
    
    func colorButtons() {
        sev0.backgroundColor = Util.multiButtonDefaultColor
        sev1.backgroundColor = Util.multiButtonDefaultColor
        sev2.backgroundColor = Util.multiButtonDefaultColor
        if severity == 0 {
            sev0.backgroundColor = Util.multiButtonSelectedColor
        }
        if severity == 1 {
            sev1.backgroundColor = Util.multiButtonSelectedColor
        }
        if severity == 2 {
            sev2.backgroundColor = Util.multiButtonSelectedColor
        }
    }
    
    @nonobjc
    func done(bp: BodyPart) {
        bodyPart.setTitle(bp.major! + " - " + bp.minor!, forState: .Normal)
        bodyPartId = bp.id
        cancel()
    }
    
    @nonobjc
    func done(it: String?) {
        if it != nil {
            injuryType.setTitle(it, forState: .Normal)
        }
        cancel()
    }
    
    func cancel() {
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    func selectBodyPartInternal() {
        State.currentIncident.majorBodyPartIndicator = nil
        let vc = BodyPartSelectorViewController()
        vc.delegate = self
        
        Util.modal(vc, self)
    }
    
    override func done(sender: UITapGestureRecognizer) {
        if type == ViewType.Create {
            injury = Injury.newObject()
        }
        
        injury.incident = State.currentIncident.id
        injury.bodyPart = bodyPartId
        injury.severity = severity
        injury.type = injuryType.titleForState(.Normal)
        injury.notes = injuryDescription.text
        
        delegate.done(injury)
    }
    
    override func cancel(sender: UITapGestureRecognizer) {
        delegate.cancel()
    }
}

