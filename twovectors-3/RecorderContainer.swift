//
//  RecorderContainer.swift
//  twovectors-3
//
//  Created by Nick Bodnar on 9/11/15.
//  Copyright (c) 2015 Nick Bodnar. All rights reserved.
//

import Foundation
import UIKit

class RecorderContainer: UIViewController, AddStatementViewControllerDelegate {
    var delegate:RecorderFinishPreviousViewControllerDelegate!
    
    @IBOutlet weak var topText: UILabel!
    @IBOutlet weak var topLeftText: UILabel!
    @IBOutlet weak var topRightText: UILabel!
    @IBOutlet weak var iconsView: UIView!
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var bottomMenu: UIView!
    @IBOutlet weak var previousButton: UIButton!
    @IBOutlet weak var nextButton: UIButton!
    @IBOutlet weak var exitButton: UIView!
    @IBOutlet weak var notesButton: UIView!
    @IBOutlet weak var audioButton: UIView!
    @IBOutlet weak var cameraButton: UIView!
    @IBOutlet weak var emergencyButton: UIView!
    @IBOutlet weak var overlayView: UIView!
    
    @IBAction func next(sender: AnyObject) {
        State.recorderVC.nextPage()
    }
    
    @IBAction func prev(sender: AnyObject) {
        State.recorderVC.prevPage()
    }
    
    @IBAction func emergency(sender: UITapGestureRecognizer) {
        let url = NSURL(string: "telprompt:911")!
        if UIApplication.sharedApplication().canOpenURL(url) {
            UIApplication.sharedApplication().openURL(url)
        } else {
            let alert = UIAlertController(title: "Cannot Use Phone", message: "", preferredStyle: .Alert)
            
            let saveAction = UIAlertAction(title: "OK", style: .Default, handler: nil)
            
            alert.addAction(saveAction)
            
            presentViewController(alert, animated: true, completion: nil)
        }
    }
    
    @IBAction func exit(sender: UITapGestureRecognizer) {
        delegate.cancel()
    }
    
    @IBAction func addNote(sender: UITapGestureRecognizer) {
        addMedia(.Text)
    }
    
    @IBAction func addAudio(sender: UITapGestureRecognizer) {
        addMedia(.Audio)
    }
    
    @IBAction func addPicture(sender: UITapGestureRecognizer) {
        addMedia(.Camera)
    }
    
    func addMedia(t:StatementType) {
        let vc = AddStatementViewController()
//        vc.modalPresentationStyle = .Popover
//        
//        vc.preferredContentSize = CGSize(width: 200, height: 200)
//        vc.popoverPresentationController?.sourceView = sender
//        vc.popoverPresentationController?.sourceRect = sender.bounds
//        vc.popoverPresentationController?.delegate = vc
//
        vc.statementType = t
        vc.viewType = .Create
        vc.delegate = self
        presentViewController(vc, animated: true, completion: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        overlayView.backgroundColor = UIColor(CGColor: UIColor.blackColor().CGColor)
        
        setMainViewController(State.recorderVC)
        updateSupervisorText()
    }
    
    func updateSupervisorText() {
        setTopLeftLabel("Supervisor: " + State.sessionUser)
    }
    
    func moveOverlay(position:Int) {
        for c in iconsView.constraints {
            if c.identifier == "position" {
                //c.multiplier = CGFloat((1.0 + Double(position) * 2.0)/6.0)
                iconsView.removeConstraint(c)
                let cc = NSLayoutConstraint(item: overlayView, attribute: .CenterX, relatedBy: .Equal, toItem: iconsView, attribute: .CenterX, multiplier: CGFloat((1.0 + Double(position) * 2.0)/6.0), constant: 0)
                cc.identifier = "position"
                iconsView.addConstraint(cc)
                break
            }
        }
        UIView.animateWithDuration(0.25) {
            self.view.layoutIfNeeded()
        }
        
        updatePageStatuses()
    }
    
    func updatePageStatuses() {
        for v in iconsView.subviews {
            if v is NavigationIconView {
                let vv = v as! NavigationIconView
                let max = Int(State.currentIncident?.highestPage ?? 0)
                if vv.index <= max {
                    let status = (State.recorderVC.getVCForIndex(vv.index) as! RecorderPage).pageStatus()
                    vv.statusImage.image = getStatusImage(status)
                } else {
                    vv.statusImage.image = getStatusImage(.None)
                }
            }
        }
    }
    
    func getStatusImage(status:RecorderPageStatus) -> UIImage? {
        switch(status) {
        case .Bad:
            return UIImage(named: "nick-red.png")
        case .Good:
            return UIImage(named: "nick-green.png")
        case .Unknown:
            return UIImage(named: "nick-orange.png")
        case .None:
            return UIImage(named: "nick-black.png")
        }
    }
    
    func setMainViewController(vc: UIViewController) {
        for v in childViewControllers {
            v.willMoveToParentViewController(nil)
            v.view.removeFromSuperview()
            v.removeFromParentViewController()
        }
        addChildViewController(vc)
        mainView.addSubview(vc.view)
        mainView.autoresizingMask = [.FlexibleWidth, .FlexibleHeight]
        
        vc.view.frame = mainView.bounds
        vc.view.autoresizingMask = [.FlexibleWidth, .FlexibleHeight]
        
        vc.didMoveToParentViewController(self)
    }
    
    func done(vc:AddStatementViewController) {
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    func cancel() {
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    // maybe this will clear search box
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        view.endEditing(true)
    }
    
    func setTopLabel(text:String?) {
        topText.text = text
    }
    
    func setTopLeftLabel(text:String?) {
        if topLeftText != nil {
            topLeftText.text = text
        }
    }
    
    func setTopRightLabel(text:String?) {
        topRightText.text = text
    }
}