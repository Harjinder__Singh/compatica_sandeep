//
//  AddDestinationViewController.swift
//  twovectors-3
//
//  Created by Nick Bodnar on 12/23/15.
//  Copyright © 2015 Nick Bodnar. All rights reserved.
//

import Foundation
import UIKit

class AddDestinationViewController: DetailViewController, EmployeeSelectorViewControllerDelegate, ClinicOrERSelectorViewControllerDelegate {
    var delegate:AddDestinationViewControllerDelegate!

    var escort:Employee?
    var clinicOrER:Destination!
    
    var clinicOrERButton = UIButton()
    var employeeName = UIButton()
    var time = UIButton()
    
    override func loadView() {
        super.loadView()
        
        if clinicOrER == .Clinic || clinicOrER == .ER {
            addButtonDetail(clinicOrERButton, clinicOrER.rawValue, "selectClinicOrER:")
            clinicOrERButton.setTitle("Tap to Select", forState: .Normal)
        }
        
        employeeName.setTitle("Tap to Select", forState: .Normal)
        time.setTitle("Tap to Select", forState: .Normal)
        
        addButtonDetail(employeeName, "Escorted By", "selectEmployee:")
        addTimeDetail(time, "Time")
        
        if State.currentIncident.destinationClinicId > 0 && clinicOrER == .Clinic {
            clinicOrERButton.setTitle(State.getClinic(State.currentIncident.destinationClinicId)?.name, forState: .Normal)
        }
        
        if State.currentIncident.destinationERId > 0 && clinicOrER == .ER {
            clinicOrERButton.setTitle(State.getER(State.currentIncident.destinationERId)?.name, forState: .Normal)
        }
        
        if State.currentIncident.destinationEscortedBy != 0 {
            employeeName.setTitle(State.getEmployee(State.currentIncident.destinationEscortedBy)?.name, forState: .Normal)
        }
        
        State.currentIncident.destinationTime = NSDate.timeIntervalSinceReferenceDate()
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        if State.currentIncident.destinationTime > 0 {
//            time.text = Util.
        }
    }
    
    func selectEmployee(sender: AnyObject) {
        let vc = EmployeeSelectorViewController()
        vc.delegate = self
        
        Util.modal(vc, self)
    }
    
    func selectClinicOrER(sender: AnyObject) {
        if clinicOrER == .Clinic {
            let vc = ClinicSelectorViewController()
            vc.delegate = self
        
            Util.modal(vc, self)
        } else if clinicOrER == .ER {
            let vc = ERSelectorViewController()
            vc.delegate = self
            
            Util.modal(vc, self)
        }
    }
    
    override func dateChanged(datePicker: UIDatePicker) {
        super.dateChanged(datePicker)
        State.currentIncident.destinationTime = datePicker.date.timeIntervalSinceReferenceDate
    }
    
    override func done(sender: UITapGestureRecognizer) {
        delegate.done(self)
    }
    
    override func cancel(sender: UITapGestureRecognizer) {
        delegate.cancel()
    }
    
    @nonobjc
    func done(employeeId:Int64) {
        State.currentIncident.destinationEscortedBy = employeeId
        employeeName.setTitle(State.getEmployee(employeeId)!.name!, forState: .Normal)
        
        cancel()
    }
    
    @nonobjc
    func done(clinicOrER clinicOrERId:Int64) {
        if clinicOrER == .Clinic {
            State.currentIncident.destinationClinicId = clinicOrERId
            clinicOrERButton.setTitle(State.getClinic(clinicOrERId)!.name!, forState: .Normal)
        } else if clinicOrER == .ER {
            State.currentIncident.destinationERId = clinicOrERId
            clinicOrERButton.setTitle(State.getER(clinicOrERId)!.name!, forState: .Normal)
        }
        
        cancel()
    }
    
    func cancel() {
        dismissViewControllerAnimated(true, completion: nil)
    }
}
