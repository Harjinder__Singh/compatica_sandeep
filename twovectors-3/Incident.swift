//
//  Incident.swift
//  twovectors-3
//
//  Created by Nick Bodnar on 10/1/15.
//  Copyright © 2015 Nick Bodnar. All rights reserved.
//

import Foundation
import CoreData

class Incident: NSManagedObject, Syncable {
        
    func toJSON() -> JSON {
        var j = JSON([:])

        j["datetime"].stringValue = Util.dateToString(time)
        
        j["employee"].int64Value = employee
        j["location"].int64Value = location
        j["zone"].int64Value = zonex
        j["equipment"].int64Value = equipment
        
        let o:[String:AnyObject] = [
            "given": firstAidGiven,
            "description": ""
        ]
        j["firstAid"].object = o

        var injuries = [AnyObject]()
        for i in getInjuries() {
            var injury = [String:AnyObject]()
            injury["bodyPart"] = i.getMajorBodyPartName()
            injury["subBodyPart"] = i.getMinorBodyPartName()
            injury["severity"] = Int(i.severity) + 1
            injury["description"] = i.notes ?? ""
            // TODO: use the actual type
//            injury["injuryType"] = type
            injury["injuryType"] = 2
            injuries.append(injury)
        }
        
        j["injuries"].arrayObject = injuries
        
        var illnesses = [AnyObject]()
        for i in getIllnesses() {
            var illness = [String:AnyObject]()
            illness["type"] = i.type ?? ""
            illness["severity"] = Int(i.severity) + 1
            illness["description"] = i.notes ?? ""
            illnesses.append(illness)
        }
        
        j["illnesses"].arrayObject = illnesses

        let ack:[String:AnyObject] = [
            "time": Util.dateToString(time),
            // TODO:
            "statement": "Not Actually Signed",
            "signature": "Not Actually Signed",
            "notes": "Not Actually Signed",
        ]
        j["injuryAcknowledgedData"].object = ack

        j["medicalAttentionRequired"].boolValue = medicalAttentionRequired
        j["medicalTreatmentRefused"].boolValue = medicalAttentionRefused
        
        if medicalAttentionRefused {
            let refusal:[String:AnyObject] = [
                "time": Util.dateToString(signatureRefusalDate),
                "statement": Util.refusalStatement,
                "signature": signatureRefusalName ?? "",
                "notes": "",
            ]
            j["medicalTreatmentRefusedData"].object = refusal
        }
        
        j["sentHome"].boolValue = destinationHome
        j["returnToWork"].boolValue = destinationWork
        j["sentToClinic"].boolValue = destinationClinic
        j["sentToER"].boolValue = destinationER
        
        if destinationHome {
            let o:[String:AnyObject] = [
                "escortedBy": Int(destinationEscortedBy),
                "time": Util.dateToString(destinationTime),
            ]
            j["sentHomeDetails"].object = o
        }
        if destinationWork {
            let o:[String:AnyObject] = [
                "escortedBy": Int(destinationEscortedBy),
                "time": Util.dateToString(destinationTime),
            ]
            j["returnToWorkDetails"].object = o
            // TODO: remove this or the one above. only for temporary compatibility
            j["returnToWorkTime"].stringValue = Util.dateToString(destinationTime)
        }
        if destinationClinic {
            let o:[String:AnyObject] = [
                "escortedBy": Int(destinationEscortedBy),
                "clinic": Int(destinationClinicId),
                "time": Util.dateToString(destinationTime),
            ]
            j["sentToClinicDetails"].object = o
        }
        if destinationER {
            let o:[String:AnyObject] = [
                "escortedBy": Int(destinationEscortedBy),
                "hospital": Int(destinationERId),
                "time": Util.dateToString(destinationTime)
            ]
            j["sentToERDetails"].object = o
        }
        
        return j
    }
    
    func compare(server: JSON) -> SyncCompare {       
        return .Different
    }
    
    func loadJSON(j: JSON) {
        id = j["id"].int64Value
        employee = j["employee_id"].int64Value
    }
    
    static func newObject() -> Incident {
        return Storage.getBlankEntity("Incident") as! Incident
    }
    
    func getEmployeeName() -> String {
        return getEmployee().name!
    }
    
    func getEmployee() -> Employee {
        return State.getEmployee(employee)!
    }
    
    func getLocation() -> Location? {
        return State.getLocation(location)
    }
    
    func getZone() -> Zone? {
        return State.getZone(zonex)
    }
    
    func getEquipment() -> Equipment? {
        return State.getEquipment(equipment)
    }
    
    func getInjuries() -> [Injury] {
        let all = State.getInjuries()
        var result = [Injury]()
        
        for i in all {
            if i.incident == id {
                result.append(i)
            }
        }
        return result
    }
    
    func getIllnesses() -> [Illness] {
        let all = State.getIllnesses()
        var result = [Illness]()
        
        for i in all {
            if i.incident == id {
                result.append(i)
            }
        }
        return result
    }
    
    func getStatements() -> [Statement] {
        let all = State.getStatements()
        var result = [Statement]()
        
        for i in all {
            if i.incident == id {
                result.append(i)
            }
        }
        return result
    }
}
