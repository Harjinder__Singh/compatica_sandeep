//
//  RecorderIllnessViewController.swift
//  twovectors-3
//
//  Created by Nick Bodnar on 12/19/15.
//  Copyright © 2015 Nick Bodnar. All rights reserved.
//

import Foundation
import UIKit

class IllnessListViewController: ObjectCollectionViewController<IllnessView>, AddIllnessViewControllerDelegate {
    
    required override init() {
        super.init()
        addButtonHidden = false
        detailBar = false
        headerText = "Illness List"
    }
    
    override func setupDataCallbacks() {
        State.setOnIllnessesUpdated(update)
    }
    
    override func updateData() -> [objectType] {
        let injuries = State.currentIncident.getIllnesses()
        
        var result = [IllnessView]()
        for i in injuries {
            let iv = IllnessView(frame: CGRectMake(0, 0, 150, 75))
            iv.illness = i
            result.append(iv)
        }
        
        return result
    }
    
    override func detail(cell: objectType) {
        let add = AddIllnessViewController()
        add.type = .Edit
        add.illness = cell.illness
        add.delegate = self
        presentViewController(add, animated: true, completion: nil)
    }
    
    override func add() {
        let add = AddIllnessViewController()
        add.type = .Create
        add.delegate = self
        presentViewController(add, animated: true, completion: nil)
    }
    
    func done(i:Illness) {
        State.addIllness(i)
        
        cancel()
        
        added()
    }
    
    func cancel() {
        dismissViewControllerAnimated(true, completion: nil)
    }
}
