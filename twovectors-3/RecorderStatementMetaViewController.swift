//
//  RecorderStatementViewController.swift
//  twovectors-3
//
//  Created by Nick Bodnar on 9/25/15.
//  Copyright © 2015 Nick Bodnar. All rights reserved.
//

import Foundation
import UIKit

class RecorderStatementMetaViewController: ObjectCollectionListViewController, RecorderPage {
    lazy var _vcs:[ObjectCollection] = {
        let oc:[ObjectCollection] = [StatementListViewController.init()]
        
        for o in oc {
            o.setParentList(self)
        }
        
        return oc
    }()
    
    override var vcs:[ObjectCollection]! {
        get {
            return _vcs
        }
    }
    
    override func tapped(cell: ObjectView) {
        for v in vcs {
            v.deselectAll()
        }
        
        (vcs[0] as! StatementListViewController).detail(cell as! StatementView)
    }
    
    func pageStatus() -> RecorderPageStatus {
        if State.currentIncident.getStatements().count == 0 {
            if State.recorderVC.viewControllers!.count == 0 || State.recorderVC.viewControllers![0] == self {
                return .Unknown
            }

            return .Bad
        }
        
        return .Good
    }
    
    func leavingPage(toPage: Int) -> Bool {
        return true
    }
}
