//
//  BodyPartSelectorViewController.swift
//  twovectors-3
//
//  Created by Nick Bodnar on 1/1/16.
//  Copyright © 2016 Nick Bodnar. All rights reserved.
//

import Foundation
import UIKit

class BodyPartSelectorViewController: ObjectCollectionListViewController {
    var delegate:BodyPartSelectorViewControllerDelegate!
    
    lazy var _vcs:[ObjectCollection] = {
        let oc:[ObjectCollection] = [MajorBodyPartListViewController.init(), MinorBodyPartListViewController.init()]
        
        for o in oc {
            o.setParentList(self)
        }
        
        return oc
    }()
    
    override var vcs:[ObjectCollection]! {
        get {
            return _vcs
        }
    }
    
    override func tapped(cell: ObjectView) {
        if let major = cell as? MajorBodyPartView {
            State.currentIncident.majorBodyPartIndicator = major.objectSelected ? major.bodyPart.major : ""
        }
        
        if let minor = cell as? MinorBodyPartView {
            delegate.done(minor.bodyPart)
            // return
        }
        
        updateCollections()
    }
}
