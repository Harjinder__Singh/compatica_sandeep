//
//  AddEmployeeViewController.swift
//  twovectors-3
//
//  Created by Nick Bodnar on 10/24/15.
//  Copyright © 2015 Nick Bodnar. All rights reserved.
//

import Foundation
import UIKit

class AddEmployeeViewController: DetailViewController {
    var delegate:AddEmployeeViewControllerDelegate!
    var employee:Employee!
    
    var image = UIImageView(image: UIImage(named: "no-image.gif"))
    var name = UITextField()
    var employeeId = UITextField()
    var jobTitle = UITextField()
    var address = UITextField()
    var city = UITextField()
    var state = UITextField()
    var zip = UITextField()
    var contactName = UITextField()
    var contactPhone = UITextField()
    var contactEmail = UITextField()
    var confirmation = UITextField()
    
    override func loadView() {
        super.loadView()
        
        addImageDetail(image, "Photo\n(Tap to Change)")
        addTextDetail(name, "Name")
        addTextDetail(employeeId, "Employee ID")
        addTextDetail(jobTitle, "Job Title")
        addTextDetail(address, "Address")
        addTextDetail(city, "City")
        addTextDetail(state, "State")
        addTextDetail(zip, "Zip Code")
        addSpace()
        addText("Emergency Contact")
        addTextDetail(contactName, "Name")
        addTextDetail(contactPhone, "Phone")
        addTextDetail(contactEmail, "Email")
        addSpace()
        addTextDetail(confirmation, "Signature")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        if type != .Create {
            name.text = employee.name
            image.image = Util.getImage(employee.media)
            employeeId.text = employee.employeeId
            jobTitle.text = employee.jobTitle
            address.text = employee.address
            city.text = employee.city
            state.text = employee.state
            zip.text = employee.zip
            contactName.text = employee.contactName
            contactPhone.text = employee.contactPhone
            contactEmail.text = employee.contactEmail
        }
    }
    
    override func done(sender: UITapGestureRecognizer) {
        var valid = false
        if let n = name.text {
            if let i = n.characters.indexOf(Character(" ")) {
                if i < n.characters.endIndex {
                    valid = true
                }
            }
        }
        
        if !valid {
            // TODO: alert user as to why
            return delegate.cancel()
        }
        
        if type == ViewType.Create {
            employee = Employee.newObject()
        }
        
        employee.name = name.text
        employee.employeeId = employeeId.text
        employee.jobTitle = jobTitle.text
        employee.address = address.text
        employee.city = city.text
        employee.state = state.text
        employee.zip = zip.text
        employee.contactName = contactName.text
        employee.contactPhone = contactPhone.text
        employee.contactEmail = contactEmail.text
        employee.companyId = Util.getCompany() ?? 0

        delegate.done(employee)
    }
    
    override func cancel(sender: UITapGestureRecognizer) {
        delegate.cancel()
    }
}