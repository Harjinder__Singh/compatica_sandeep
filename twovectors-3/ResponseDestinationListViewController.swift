//
//  ResponseDestinationListViewController.swift
//  twovectors-3
//
//  Created by Nick Bodnar on 12/21/15.
//  Copyright © 2015 Nick Bodnar. All rights reserved.
//

import Foundation
import UIKit

class ResponseDestinationListViewController: ObjectCollectionViewController<ResponseDestinationView> {
    var defaultHeader = "Employee Destination"
    
    required override init() {
        super.init()
        headerText = defaultHeader
    }
    
    override func updateData() -> [objectType] {
        var responses = [Destination]()
        
        if State.currentIncident.medicalAttentionRequired {
            responses.append(.Clinic)
            responses.append(.ER)
        }
        
        if State.currentIncident.medicalAttentionNotRequired {
            responses.append(.Work)
            responses.append(.Home)
        }
        
        headerText = responses.isEmpty ? "" : defaultHeader
        
        var result = [ResponseDestinationView]()
        for r in responses {
            let rv = ResponseDestinationView(frame: CGRectMake(0, 0, 150, 75))
            rv.destination = r
            if State.currentIncident.destinationWork && r == .Work {
                rv.objectSelected = true
                result = [ResponseDestinationView]()
                result.append(rv)
                return result
            }
            if State.currentIncident.destinationHome && r == .Home {
                rv.objectSelected = true
                result = [ResponseDestinationView]()
                result.append(rv)
                return result
            }
            if State.currentIncident.destinationClinic && r == .Clinic {
                rv.objectSelected = true
                result = [ResponseDestinationView]()
                result.append(rv)
                return result
            }
            if State.currentIncident.destinationER && r == .ER {
                rv.objectSelected = true
                result = [ResponseDestinationView]()
                result.append(rv)
                return result
            }
            result.append(rv)
        }
        
        return result
    }
}
