//
//  ModalViewController.swift
//  twovectors-3
//
//  Created by Nick Bodnar on 1/16/16.
//  Copyright © 2016 Nick Bodnar. All rights reserved.
//

import Foundation
import UIKit

class ModalViewController:UIViewController {
    @IBOutlet weak var container: UIView!
    
    var delegate:CancelDelegate?
    
    @IBAction func tapped(sender: UITapGestureRecognizer) {
        let point = sender.locationInView(container)
        if container.pointInside(point, withEvent: nil) {
            return
        }
        
        delegate?.cancel()
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.layoutIfNeeded()
    }
    
    override func viewWillAppear(animated: Bool) {
        view.layoutIfNeeded()
        super.viewWillAppear(animated)
    }
}
