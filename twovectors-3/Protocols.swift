//
//  Delegates.swift
//  twovectors-3
//
//  Created by Nick Bodnar on 12/4/15.
//  Copyright © 2015 Nick Bodnar. All rights reserved.
//

import Foundation
import UIKit
import CoreData

protocol CancelDelegate {
    func cancel()
}

protocol DoneCancelDelegate:CancelDelegate {
    func done()
}

protocol RecorderFinishPreviousViewControllerDelegate:CancelDelegate {
}

protocol IncidentSelectorViewControllerDelegate:CancelDelegate {
    func done(incidentId: Int64)
}

protocol AddEmployeeViewControllerDelegate:CancelDelegate {
    func done(e:Employee)
}

protocol AddLocationViewControllerDelegate:CancelDelegate {
    func done(l:Location)
}

protocol AddZoneViewControllerDelegate:CancelDelegate {
    func done(z:Zone)
}

protocol AddEquipmentViewControllerDelegate:CancelDelegate {
    func done(e:Equipment)
}

protocol AddInjuryViewControllerDelegate:CancelDelegate {
    func done(injury:Injury)
}

protocol BodyPartSelectorViewControllerDelegate:CancelDelegate {
    func done(bodyPart: BodyPart)
}

protocol InjuryTypeSelectorViewControllerDelegate:CancelDelegate {
    func done(injuryType: String?)
}

protocol IllnessTypeSelectorViewControllerDelegate:CancelDelegate {
    func done(illnessType: String?)
}

protocol EmployeeSelectorViewControllerDelegate:CancelDelegate {
    func done(employeeId:Int64)
}

protocol AddIllnessViewControllerDelegate:CancelDelegate {
    func done(illness:Illness)
}

protocol AddResponseDetailViewControllerDelegate:CancelDelegate {
    func done(vc:AddResponseDetailViewController)
}

protocol AddDestinationViewControllerDelegate:CancelDelegate {
    func done(vc:AddDestinationViewController)
}

protocol AddStatementViewControllerDelegate:CancelDelegate {
    func done(vc:AddStatementViewController)
}

protocol AddSignatureViewControllerDelegate:CancelDelegate {
    func done(sig:AddSignatureViewController)
}

protocol AddAudioViewControllerDelegate:CancelDelegate {
    func done(vc:AddAudioViewController)
}

protocol ClinicOrERSelectorViewControllerDelegate:CancelDelegate {
    func done(clinicOrER clinicOrERId:Int64)
}

protocol GetSignatureViewControllerDelegate:CancelDelegate {
    func done(e:GetSignatureViewController)
}

protocol RecorderPage {
    func pageStatus() -> RecorderPageStatus
    func leavingPage(_: Int) -> Bool
}

protocol ObjectCollectionList {
    func tapped(cell: ObjectView)
    func resize()
}

protocol ObjectCollection {
    func setupDataCallbacks()
    func update()
    func layout()
    func deselectAll()
    func getHeight() -> CGFloat
    func setFrame(f: CGRect)
    func setParentList(list:ObjectCollectionList)
    func getParentList() -> ObjectCollectionList
}

protocol AbstractObjectCollection: ObjectCollection {
    typealias objectType
    func updateData() -> [objectType]
    func add()
}

protocol Syncable {
    typealias objectType
    func toJSON() -> JSON
    static func newObject() -> objectType
    func loadJSON(j:JSON)
    func compare(server:JSON) -> SyncCompare
}
